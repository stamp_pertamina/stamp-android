package com.devmatech.www.stamp.fragment;

/**
 * Created by Whelly on 31/05/2017.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devmatech.www.stamp.R;

public class TabFragment extends Fragment {

    private int position;
    private TextView content;
    private ImageView image;

    public static Fragment getInstance(int position) {
        TabFragment f = new TabFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //get data from Argument
        position = getArguments().getInt("position");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tabs_detil_strategic, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        image = (ImageView) view.findViewById(R.id.image);
        content = (TextView) view.findViewById(R.id.textView);
        setContentView();
    }

    private void setContentView() {
        if (position == 0) {
            //food fragment
//            image.setImageResource(R.drawable.ic_menu_manage);
            content.setText("To Take over Production Sharing Contract at the Mahakam Block, East Kalimantan. The smooth take over allows the company to acquire the block earlier.");
        } else if (position == 1) {
            //movie fragment
//            image.setImageResource(R.drawable.ic_menu_send);
            content.setText("Mahakam Block is one of the country's largest gas reserves. The takeover can potentially increase Pertamana gas production by up to 280,000 barrels of oil perday.");
        } else if (position == 2) {
            //shopping fragment
//            image.setImageResource(R.drawable.ic_menu_camera);
            content.setText("This is Agenda Person");
        }
    }
}
