package com.devmatech.www.stamp.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS-PC on 16/06/2017.
 */

public class SOModel implements Serializable {
    private String id;

    private List<PrioritizationMatrix> prioritization_matrix;

    private List<InternalSmes> internal_smes;

    private String so_goal;

    private List<EngagementTimeline> engagement_timeline;

    private List<Pic> pic;

    private String so_description;

    private String so_name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public List<PrioritizationMatrix> getPrioritizationMatrix ()
    {
        return prioritization_matrix;
    }

    public void setPrioritizationMatrix (List<PrioritizationMatrix> prioritization_matrix)
    {
        this.prioritization_matrix = prioritization_matrix;
    }

    public List<InternalSmes> getInternal_smes ()
    {
        return internal_smes;
    }

    public void setInternalSmes (List<InternalSmes> internal_smes)
    {
        this.internal_smes = internal_smes;
    }

    public String getSo_goal ()
    {
        return so_goal;
    }

    public void setSo_goal (String so_goal)
    {
        this.so_goal = so_goal;
    }

    public List<EngagementTimeline> getEngagement_timeline ()
    {
        return engagement_timeline;
    }

    public void setEngagement_timeline (List<EngagementTimeline> engagement_timeline)
    {
        this.engagement_timeline = engagement_timeline;
    }

    public List<Pic> getPic ()
    {
        return pic;
    }

    public void setPic (List<Pic> pic)
    {
        this.pic = pic;
    }

    public String getSo_description ()
    {
        return so_description;
    }

    public void setSo_description (String so_description)
    {
        this.so_description = so_description;
    }

    public String getSo_name ()
    {
        return so_name;
    }

    public void setSo_name (String so_name)
    {
        this.so_name = so_name;
    }
}