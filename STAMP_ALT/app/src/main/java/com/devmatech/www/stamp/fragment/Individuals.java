package com.devmatech.www.stamp.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.activity.DetailIndividual;
import com.devmatech.www.stamp.adapter.IndividualListAdapter;
import com.devmatech.www.stamp.models.IPModel;
import com.devmatech.www.stamp.models.IndividualsProfile;
import com.devmatech.www.stamp.other.Helper;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.devmatech.www.stamp.R.id;
import static com.devmatech.www.stamp.R.id.ib_close;
import static com.devmatech.www.stamp.R.id.tableRow10;
import static com.devmatech.www.stamp.R.id.tableRow11;
import static com.devmatech.www.stamp.R.id.tableRow2;
import static com.devmatech.www.stamp.R.id.tableRow4;
import static com.devmatech.www.stamp.R.id.tableRow5;
import static com.devmatech.www.stamp.R.id.tableRow6;
import static com.devmatech.www.stamp.R.id.tableRow7;
import static com.devmatech.www.stamp.R.id.tableRow8;
import static com.devmatech.www.stamp.R.id.tableRow9;
import static com.devmatech.www.stamp.R.menu;

/**
 * Created by Dimas on 30/05/2017.
 */

public class Individuals extends Fragment {

    RelativeLayout view;
    ArrayList<IPModel> itemList = new ArrayList<>();
    ListView listView;
    IndividualListAdapter adapter;
    Button btn_showAll;
    BottomNavigationView bottomNavigationView;
    private Context mContext;
    private PopupWindow mPopupWindow;
    private LinearLayout mRelativeLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = (RelativeLayout) inflater.inflate(R.layout.individuals_main, container, false);
        listView = (ListView)view.findViewById(R.id.list_individual);
        btn_showAll = (Button)view.findViewById(R.id.btn_showAll);
        bottomNavigationView = (BottomNavigationView) view.findViewById(R.id.bottomMenuIndividuals );

        mContext = getActivity();
        mRelativeLayout = (LinearLayout) view.findViewById(R.id.list_profile);
        getActivity().setTitle("Individuals Profile");

        listProfile();

        adapter = new IndividualListAdapter(itemList, getActivity().getBaseContext());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity().getBaseContext(), DetailIndividual.class);
                String nama = itemList.get(position).getIndividualName();
                intent.putExtra("profile",itemList.get(position));
                startActivity(intent);
            }
        });

        btn_showAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.showAll();
                btn_showAll.setVisibility(View.GONE);
            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener(){

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case id.menu_strategic_list:

//                        PopupMenu popup = new PopupMenu(getActivity(),  bottomNavigationView);
//                        // Inflating menu using xml file
//                        popup.getMenuInflater().inflate(menu.strategic_menu, popup.getMenu());
//
//                        // registering OnMenuItemClickListener
//                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                Toast.makeText(getActivity(),
//                                        "Kamu telah memilih : " + item.getTitle(),
//                                        Toast.LENGTH_SHORT).show();
//                                return true;
//                            }
//                        });
//                        popup.show();

                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
//
                        // Inflate the custom layout/view
                        View customView = inflater.inflate(R.layout.str_menu,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView,
                                ViewPager.LayoutParams.MATCH_PARENT,
                                ViewPager.LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button
                        ImageButton closeButton = (ImageButton) customView.findViewById( ib_close);

                        final TableRow tes = (TableRow) customView.findViewById(tableRow8) ;
                        final TableRow tes1 = (TableRow) customView.findViewById(tableRow2) ;
                        final TableRow tes3 = (TableRow) customView.findViewById(tableRow4) ;
                        final TableRow tes4 = (TableRow) customView.findViewById(tableRow5) ;
                        final TableRow tes5 = (TableRow) customView.findViewById(tableRow6) ;
                        final TableRow tes6 = (TableRow) customView.findViewById(tableRow7) ;
                        final TableRow tes8 = (TableRow) customView.findViewById(tableRow10) ;
                        final TableRow tes9 = (TableRow) customView.findViewById(tableRow11) ;

                        // Set a click listener for the popup window close button
                        tes.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                Toast.makeText(getActivity(),
                                        "Kamu telah memilih ini : Maintaining Domestic Oil and Gas Production" ,
                                        Toast.LENGTH_SHORT).show();
                                tes.setBackgroundColor( 000 );
                                mPopupWindow.dismiss();

                            }
                        });tes1.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih ini : Good Coorporate Governance" ,
                                    Toast.LENGTH_SHORT).show();
                            tes1.setBackgroundColor( 000 );
                            mPopupWindow.dismiss();

                        }
                    });tes3.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih ini : Companys Value Improvement" ,
                                    Toast.LENGTH_SHORT).show();
                            tes3.setBackgroundColor( 000 );
                            mPopupWindow.dismiss();

                        }
                    });tes4.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih ini : Ensuring Stable Cash Flow" ,
                                    Toast.LENGTH_SHORT).show();
                            tes4.setBackgroundColor( 000 );
                            mPopupWindow.dismiss();

                        }
                    });tes5.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih ini : Effectice Utilization of CSR found" ,
                                    Toast.LENGTH_SHORT).show();
                            tes5.setBackgroundColor( 000 );
                            mPopupWindow.dismiss();

                        }
                    });tes6.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih ini : Support on Energy Holding Establishment" ,
                                    Toast.LENGTH_SHORT).show();
                            tes6.setBackgroundColor( 000 );
                            mPopupWindow.dismiss();

                        }
                    });tes8.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih ini : Oil and Gas Block Acquisition" ,
                                    Toast.LENGTH_SHORT).show();
                            tes8.setBackgroundColor( 000 );
                            mPopupWindow.dismiss();

                        }
                    });tes9.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih ini : Mahakam Block Takeover" ,
                                    Toast.LENGTH_SHORT).show();
                            tes9.setBackgroundColor( 000 );
                            mPopupWindow.dismiss();

                        }
                    });

                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);

                        break;
                    case id.menu_sort:
//                        PopupMenu popups = new PopupMenu(getActivity(),  bottomNavigationView);
//                        // Inflating menu using xml file
//                        popups.getMenuInflater().inflate(menu.sort_menu, popups.getMenu());
//
//                        // registering OnMenuItemClickListener
//                        popups.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                Toast.makeText(getActivity(),
//                                        "Kamu telah memilih : " + item.getTitle(),
//                                        Toast.LENGTH_SHORT).show();
//                                return true;
//                            }
//                        });
//                        popups.show();

                        LayoutInflater inflater2 = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
//
                        // Inflate the custom layout/view
                        View customView2 = inflater2.inflate(R.layout.sort_menu,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView2,
                                ViewPager.LayoutParams.MATCH_PARENT,
                                ViewPager.LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button

                        final TableRow tes0 = (TableRow) customView2.findViewById(tableRow8) ;
                        final TableRow tes01 = (TableRow) customView2.findViewById(tableRow2) ;
                        final TableRow tes02 = (TableRow) customView2.findViewById(tableRow4) ;

                        // Set a click listener for the popup window close button
                        tes0.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                Toast.makeText(getActivity(),
                                        "Kamu telah memilih ini : Date Added(New to Old)",
                                        Toast.LENGTH_SHORT).show();
                                tes0.setBackgroundColor( 000 );
                                mPopupWindow.dismiss();

                            }
                        });tes01.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih ini : Date Added(Old to New)" ,
                                    Toast.LENGTH_SHORT).show();
                            tes01.setBackgroundColor( 000 );
                            mPopupWindow.dismiss();

                        }
                    });tes02.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih ini : Name(A to Z)",
                                    Toast.LENGTH_SHORT).show();
                            tes02.setBackgroundColor( 000 );
                            mPopupWindow.dismiss();

                        }
                    });

                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);

                        break;
                }
                return true;
            }
        } );

        return view;
    }

    private void listProfile(){
        Gson gson = new Gson();
        Helper helper = new Helper();
        try {
            JSONArray array = new JSONArray(helper.LoadJsonDataFromAssets(getActivity(), "IP.json"));
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                IPModel ipModel = gson.fromJson(obj.toString(), IPModel.class);
                itemList.add(ipModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
