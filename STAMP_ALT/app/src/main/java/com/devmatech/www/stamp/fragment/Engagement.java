package com.devmatech.www.stamp.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.Toast;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.adapter.LegendListAdapter;
import com.devmatech.www.stamp.models.LegendList;
import com.devmatech.www.stamp.other.Remover;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.devmatech.www.stamp.R.id.tableRow1;
import static com.devmatech.www.stamp.R.id.tableRow2;
import static com.devmatech.www.stamp.R.id.tableRow3;
import static com.devmatech.www.stamp.R.id.tableRow4;
import static com.devmatech.www.stamp.R.id.tableRow5;
import static com.devmatech.www.stamp.R.id.tableRow8;

/**
 * Created by Dimas on 30/05/2017.
 */

public class Engagement extends Fragment implements OnChartValueSelectedListener {

    public Engagement(){}
//    RelativeLayout view;
    private Spinner spinner1;
    private GridView gridView,gridView2,gridView3,gridView4;
    private LinearLayout linlay;
    PieChart pieChart,pieChart2,pieChart3,pieChart4 ;
    ArrayList<Entry> entries,entries_char2,entries_char3,entries_char4 ;
    ArrayList<String> PieEntryLabels,PieEntryLabels_char2,PieEntryLabels_char3,PieEntryLabels_char4 ;
    PieDataSet pieDataSet,pieDataSetChar2,pieDataSetChar3,pieDataSetChar4 ;
    PieData pieData,pieDataChar2,pieDataChar3,pieDataChar4 ;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Fragment fragment = null;

    ArrayList<LegendList> LegendsList = new ArrayList<>();
    ArrayList<LegendList> LegendsList2 = new ArrayList<>();
    ArrayList<LegendList> LegendsList3 = new ArrayList<>();
    ArrayList<LegendList> LegendsList4 = new ArrayList<>();
    LegendListAdapter adapterLegend,adapterLegend2,adapterLegend3,adapterLegend4;
    private Context mContext;
    private PopupWindow mPopupWindow;
    private LinearLayout mRelativeLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.engagement_main, container, false);

        gridView = (GridView) view.findViewById(R.id.grid_view);
        gridView2 = (GridView) view.findViewById(R.id.grid_view2);
        gridView3 = (GridView) view.findViewById(R.id.grid_view3);
        gridView4 = (GridView) view.findViewById(R.id.grid_view4);

        listLegendChart1();
        listLegendChart2();
        listLegendChart3();
        listLegendChart4();
        adapterLegend = new LegendListAdapter(LegendsList, getActivity().getBaseContext());
        adapterLegend2 = new LegendListAdapter(LegendsList2, getActivity().getBaseContext());
        adapterLegend3 = new LegendListAdapter(LegendsList3, getActivity().getBaseContext());
        adapterLegend4 = new LegendListAdapter(LegendsList4, getActivity().getBaseContext());

        gridView.setAdapter(adapterLegend);
        gridView2.setAdapter(adapterLegend2);
        gridView3.setAdapter(adapterLegend3);
        gridView4.setAdapter(adapterLegend4);

        spinner1 = (Spinner) view.findViewById(R.id.spinner);
        spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());

        mContext = getActivity();
        mRelativeLayout = (LinearLayout) view.findViewById(R.id.cover);

        getActivity().setTitle("Engagement Summary");

//        linlay = (LinearLayout) view.findViewById(R.id.layout_sub);
//        linlay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String idd = String.valueOf(v.getId());
//                Log.i("msg", idd+"-"+R.id.layout_sub);
//                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.Detail_Engagement.class);
//                myIntent.putExtra("id", "c1");
//                startActivity(myIntent);
//                //for ipay
//
//            }
//        });
//
//        linlay = (LinearLayout) view.findViewById(R.id.layout_sub_sub);
//        linlay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String idd = String.valueOf(v.getId());
//                Log.i("msg", idd+"-"+R.id.layout_sub_sub);
//                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.Detail_Engagement.class);
//                myIntent.putExtra("id", "c2");
//                startActivity(myIntent);
//                //for ipay
//
//            }
//        });
//
//        linlay = (LinearLayout) view.findViewById(R.id.layout_sub_sub_sub);
//        linlay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String idd = String.valueOf(v.getId());
//                Log.i("msg", idd+"-"+R.id.layout_sub_sub);
//                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.Detail_Engagement.class);
//                myIntent.putExtra("id", "c3");
//                startActivity(myIntent);
//                //for ipay
//
//            }
//        });


//        linlay = (LinearLayout) view.findViewById(R.id.layout_sub_sub_sub_sub);
//        linlay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String idd = String.valueOf(v.getId());
//                Log.i("msg", idd+"-"+R.id.layout_sub_sub_sub);
//                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.Detail_Engagement.class);
//                myIntent.putExtra("id", "c4");
//                startActivity(myIntent);
//                //for ipay
//
//            }
//        });

        //piechart 1
        pieChart = (PieChart) view.findViewById(R.id.chart1);
        pieChart.setHoleRadius(40f);
        pieChart.setDescription("");
        pieChart.setCenterText("View\nDetails");
        pieChart.setDrawCenterText(true);
        pieChart.setClickable(true);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
//                Log.i("I clicked ons", String.valueOf(e.getXIndex()) );
                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.Detail_Engagement.class);
                myIntent.putExtra("id", "c1");
                startActivity(myIntent);
            }

            @Override
            public void onNothingSelected() {

            }
        });
        entries = new ArrayList<>();
        PieEntryLabels = new ArrayList<String>();
        AddValuesToPIEENTRY();
        AddValuesToPieEntryLabels();

        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new Remover(new DecimalFormat("###,###,###")));
        pieDataSet.setColors(new int[] {Color.rgb(0,108,183), Color.rgb(171,194,44), Color.rgb(237,27,46), Color.rgb(101,186,245), Color.rgb(146,152,111), Color.rgb(139,195,74), Color.rgb(34,149,136), Color.rgb(6,32,21), Color.rgb(180,180,180)});

        pieChart.setDrawSliceText(false);

        pieData.setValueTextSize(12f);
        pieData.setValueTextColor(Color.rgb(255,255,255));
        Legend legend = pieChart.getLegend();
        legend.setEnabled(false);
        legend.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        legend.setXEntrySpace(7f);
        legend.setYEntrySpace(0f);
        legend.setYOffset(0f);
        legend.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
        legend.setWordWrapEnabled(true);


        pieChart.setData(pieData);

        //pieChart.animateY(3000);

        //Pie Chart2
        pieChart2 = (PieChart) view.findViewById(R.id.chart2);
        pieChart2.setHoleRadius(40f);
        pieChart2.setDescription("");
        pieChart2.setCenterText("View\nDetails");
        pieChart2.setDrawCenterText(true);
        pieChart2.setClickable(true);
        pieChart2.setDrawHoleEnabled(true);
        pieChart2.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
//                Log.i("I clicked ons", String.valueOf(e.getXIndex()) );
                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.Detail_Engagement.class);
                myIntent.putExtra("id", "c2");
                startActivity(myIntent);
            }

            @Override
            public void onNothingSelected() {

            }
        });
        entries_char2 = new ArrayList<>();
        PieEntryLabels_char2 = new ArrayList<>();
        AddValuesToPIEENTRYCHAR2();
        AddValuesToPieEntryLabelsChar2();
        pieDataSetChar2 = new PieDataSet(entries_char2, "");

        pieDataChar2 = new PieData(PieEntryLabels_char2, pieDataSetChar2);
        pieDataChar2.setValueFormatter(new Remover(new DecimalFormat("###,###,###")));
        pieDataSetChar2.setColors(new int[] {Color.rgb(0,108,183), Color.rgb(34,149,136), Color.rgb(171,194,44), Color.rgb(6,32,21), Color.rgb(237,27,46), Color.rgb(175,175,175), Color.rgb(101,186,245), Color.rgb(225,126,126), Color.rgb(146,152,111)});

        pieChart2.setDrawSliceText(false);

        pieDataChar2.setValueTextSize(12f);
        pieDataChar2.setValueTextColor(Color.rgb(255,255,255));
        Legend legendchar2 = pieChart2.getLegend();
        legendchar2.setEnabled(false);
        legendchar2.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        legendchar2.setXEntrySpace(7f);
        legendchar2.setYEntrySpace(0f);
        legendchar2.setYOffset(0f);
        legendchar2.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
        legendchar2.setWordWrapEnabled(true);


        pieChart2.setData(pieDataChar2);

        //pieChart2.animateY(3000);


        //Pie Chart3
        pieChart3 = (PieChart) view.findViewById(R.id.chart3);
        pieChart3.setHoleRadius(40f);
        pieChart3.setDescription("");
        pieChart3.setCenterText("View\nDetails");
        pieChart3.setDrawCenterText(true);
        pieChart3.setClickable(true);
        pieChart3.setDrawHoleEnabled(true);
        pieChart3.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
//                Log.i("I clicked ons", String.valueOf(e.getXIndex()) );
                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.Detail_Engagement.class);
                myIntent.putExtra("id", "c3");
                startActivity(myIntent);
            }

            @Override
            public void onNothingSelected() {

            }
        });
        entries_char3 = new ArrayList<>();
        PieEntryLabels_char3 = new ArrayList<>();
        AddValuesToPIEENTRYCHAR3();
        AddValuesToPieEntryLabelsChar3();
        pieDataSetChar3 = new PieDataSet(entries_char3, "");

        pieDataChar3 = new PieData(PieEntryLabels_char3, pieDataSetChar3);
        pieDataChar3.setValueFormatter(new Remover(new DecimalFormat("###,###,###")));
        pieDataSetChar3.setColors(new int[] {Color.rgb(0,108,183), Color.rgb(171,149,44), Color.rgb(237,27,46), Color.rgb(101,186,245), Color.rgb(146,152,111), Color.rgb(74,145,200), Color.rgb(34,149,136), Color.rgb(6,32,21), Color.rgb(175,175,175)});

        pieChart3.setDrawSliceText(false);

        pieDataChar3.setValueTextSize(12f);
        pieDataChar3.setValueTextColor(Color.rgb(255,255,255));
        Legend legendchar3 = pieChart3.getLegend();
        legendchar3.setEnabled(false);
        legendchar3.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        legendchar3.setXEntrySpace(7f);
        legendchar3.setYEntrySpace(0f);
        legendchar3.setYOffset(0f);
        legendchar3.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
        legendchar3.setWordWrapEnabled(true);


        pieChart3.setData(pieDataChar3);

        //pieChart3.animateY(3000);


        //Pie Chart4
        pieChart4 = (PieChart) view.findViewById(R.id.chart4);
        pieChart4.setHoleRadius(40f);
        pieChart4.setDescription("");
        pieChart4.setCenterText("View\nDetails");
        pieChart4.setDrawCenterText(true);
        pieChart4.setClickable(true);
        pieChart4.setDrawHoleEnabled(true);
        pieChart4.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
//                Log.i("I clicked ons", String.valueOf(e.getXIndex()) );
                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.Detail_Engagement.class);
                myIntent.putExtra("id", "c4");
                startActivity(myIntent);
            }

            @Override
            public void onNothingSelected() {

            }
        });
        entries_char4 = new ArrayList<>();
        PieEntryLabels_char4 = new ArrayList<>();
        AddValuesToPIEENTRYCHAR4();
        AddValuesToPieEntryLabelsChar4();
        pieDataSetChar4 = new PieDataSet(entries_char4, "");

        pieDataChar4 = new PieData(PieEntryLabels_char4, pieDataSetChar4);
        pieDataChar4.setValueFormatter(new Remover(new DecimalFormat("###,###,###")));
        pieDataSetChar4.setColors(new int[] {Color.rgb(0,108,183), Color.rgb(171,149,44), Color.rgb(237,27,46), Color.rgb(101,186,245), Color.rgb(146,152,111), Color.rgb(74,145,200), Color.rgb(34,149,136), Color.rgb(191,32,21)});

        pieChart4.setDrawSliceText(false);

        pieDataChar4.setValueTextSize(12f);
        pieDataChar4.setValueTextColor(Color.rgb(255,255,255));
        Legend legendchar4 = pieChart4.getLegend();
        legendchar4.setEnabled(false);
        legendchar4.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        legendchar4.setXEntrySpace(7f);
        legendchar4.setYEntrySpace(0f);
        legendchar4.setYOffset(0f);
        legendchar4.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
        legendchar4.setWordWrapEnabled(true);


        pieChart4.setData(pieDataChar4);

        //pieChart4.animateY(3000);

        final BottomNavigationView bottomNavigationView = (BottomNavigationView) view.findViewById( R.id.bottomMenuEngagement );

        bottomNavigationView.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener(){

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()){
                    case R.id.menu_all:

//                        PopupMenu popup = new PopupMenu(getActivity(),  bottomNavigationView);
//                        // Inflating menu using xml file
//                        popup.getMenuInflater().inflate(R.menu.all_scf_menu, popup.getMenu());
//
//                        // registering OnMenuItemClickListener
//                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                fragmentManager = getFragmentManager();
//                                switch (item.getTitle().toString()){
//                                    case "Institutional Relation" :
//                                        fragment = new Engagement();
//                                        callFragment(fragment);
//                                        break;
//                                    case "Investor Relation" :
//                                        fragment = new EngagementInvestor();
//                                        callFragment(fragment);
//                                        break;
//                                    case "International" :
//                                        fragment = new EngagementInternational();
//                                        callFragment(fragment);
//                                        break;
//                                    case "Media" :
//                                        fragment = new EngagementMedia();
//                                        callFragment(fragment);
//                                        break;
//                                    case "CSR" :
//                                        fragment = new EngagementCsr();
//                                        callFragment(fragment);
//                                        break;
//                                }
//                                return true;
//                            }
//                        });
//                        popup.show();

                        //-----------------------------------------------------------------------
                        LayoutInflater inflater2 = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
//
                        // Inflate the custom layout/view
                        View customView2 = inflater2.inflate(R.layout.all_menu_eng,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView2,
                                ViewPager.LayoutParams.MATCH_PARENT,
                                ViewPager.LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button
                        fragmentManager = getFragmentManager();
                        final TableRow tes1 = (TableRow) customView2.findViewById(tableRow1) ;
                        final TableRow tes2 = (TableRow) customView2.findViewById(tableRow2) ;
                        final TableRow tes3 = (TableRow) customView2.findViewById(tableRow3) ;
                        final TableRow tes4 = (TableRow) customView2.findViewById(tableRow4) ;
                        final TableRow tes5 = (TableRow) customView2.findViewById(tableRow5) ;

                        // Set a click listener for the popup window close button
                        tes1.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                fragment = new Engagement();
                                callFragment(fragment);
                                mPopupWindow.dismiss();

                            }
                        });tes2.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                                fragment = new EngagementInvestor();
                                callFragment(fragment);
                                mPopupWindow.dismiss();

                            }
                        });tes3.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                fragment = new EngagementInternational();
                                callFragment(fragment);
                                mPopupWindow.dismiss();

                            }
                        });tes4.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                fragment = new EngagementMedia();
                                callFragment(fragment);
                                mPopupWindow.dismiss();

                            }
                        });tes5.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                fragment = new EngagementCsr();
                                callFragment(fragment);
                                mPopupWindow.dismiss();

                            }
                        });



                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);
                        break;
                    case R.id.menu_strategic:
//                        PopupMenu popup_strategic = new PopupMenu(getActivity(),  bottomNavigationView);
//                        // Inflating menu using xml file
//                        popup_strategic.getMenuInflater().inflate(R.menu.strategic_objectives_menu, popup_strategic.getMenu());
//
//                        // registering OnMenuItemClickListener
//                        popup_strategic.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                Toast.makeText(getActivity(),
//                                        "Kamu telah memilih : " + item.getTitle(),
//                                        Toast.LENGTH_SHORT).show();
//                                return true;
//                            }
//                        });
//                        popup_strategic.show();

                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
//
                        // Inflate the custom layout/view
                        View customView = inflater.inflate(R.layout.str_menu_eng,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView,
                                ViewPager.LayoutParams.MATCH_PARENT,
                                ViewPager.LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button
                        final TableRow tes_1 = (TableRow) customView.findViewById(tableRow1) ;
                        final TableRow tes_2 = (TableRow) customView.findViewById(tableRow2) ;

                        // Set a click listener for the popup window close button
                        tes_1.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                Toast.makeText(getActivity(),
                                        "Kamu telah memilih : Take Over Indonesia Blocks",
                                        Toast.LENGTH_SHORT).show();
                                mPopupWindow.dismiss();

                            }
                        });tes_2.setOnClickListener(new View.OnClickListener() {
                        @Override

                            public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih : Grass Root Refinery(GRR) Tuban",
                                    Toast.LENGTH_SHORT).show();
                                mPopupWindow.dismiss();

                            }
                        });



                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);


                        break;
                    case R.id.menu_periode:
//                        PopupMenu popup_periode = new PopupMenu(getActivity(),  bottomNavigationView);
//                        // Inflating menu using xml file
//                        popup_periode.getMenuInflater().inflate(R.menu.engagement_periode_menu, popup_periode.getMenu());
//
//                        // registering OnMenuItemClickListener
//                        popup_periode.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                Toast.makeText(getActivity(),
//                                        "Kamu telah memilih : " + item.getTitle(),
//                                        Toast.LENGTH_SHORT).show();
//                                return true;
//                            }
//                        });
//                        popup_periode.show();

                        LayoutInflater inflater3 = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
//
                        // Inflate the custom layout/view
                        View customView3 = inflater3.inflate(R.layout.per_menu_eng,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView3,
                                ViewPager.LayoutParams.MATCH_PARENT,
                                ViewPager.LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button
                        final TableRow tes1_ = (TableRow) customView3.findViewById(tableRow1) ;
                        final TableRow tes2_ = (TableRow) customView3.findViewById(tableRow2) ;
                        final TableRow tes3_ = (TableRow) customView3.findViewById(tableRow3) ;

                        // Set a click listener for the popup window close button
                        tes1_.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                Toast.makeText(getActivity(),
                                        "Kamu telah memilih : 1 Apr 2017 - 30 Apr 2017",
                                        Toast.LENGTH_SHORT).show();
                                mPopupWindow.dismiss();

                            }
                        });tes2_.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih : 1 Mar 2017 - 31 Mar 2017",
                                    Toast.LENGTH_SHORT).show();
                            mPopupWindow.dismiss();

                            }
                        });tes3_.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih : 1 Feb 2017 - 28 Feb 2017",
                                    Toast.LENGTH_SHORT).show();
                            mPopupWindow.dismiss();

                            }
                        });



                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);

                        break;
                }
                return true;
            }
        } );

        return view;
    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        Log.i("I clicked on", String.valueOf(e.getXIndex()) );
    }

    @Override
    public void onNothingSelected() {

    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        String firstItem = String.valueOf(spinner1.getSelectedItem());

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (firstItem.equals(String.valueOf(spinner1.getSelectedItem()))) {
                // ToDo when first item is selected
            } else {
                Toast.makeText(getActivity(),
                        "You have selected : " + parent.getItemAtPosition(pos).toString(),
                        Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg) {

        }

    }

    public void AddValuesToPIEENTRY(){

        entries.add(new BarEntry(15f, 0));
        entries.add(new BarEntry(14f, 1));
        entries.add(new BarEntry(12f, 2));
        entries.add(new BarEntry(9f, 3));
        entries.add(new BarEntry(9f, 4));
        entries.add(new BarEntry(6f, 5));
        entries.add(new BarEntry(6f, 6));
        entries.add(new BarEntry(6f, 7));
        entries.add(new BarEntry(23f, 9));

    }

    public void AddValuesToPieEntryLabels(){

        PieEntryLabels.add("Ministry of BUMN");
        PieEntryLabels.add("Ministry of ESDM");
        PieEntryLabels.add("DPR");
        PieEntryLabels.add("MPR");
        PieEntryLabels.add("Presiden of RI");
        PieEntryLabels.add("Vice Presiden of RI");
        PieEntryLabels.add("Saudi Aramco");
        PieEntryLabels.add("Total E & P");
        PieEntryLabels.add("Other");
    }

    private void AddValuesToPIEENTRYCHAR2() {
        entries_char2.add(new BarEntry(16f, 0));
        entries_char2.add(new BarEntry(15f, 1));
        entries_char2.add(new BarEntry(11f, 2));
        entries_char2.add(new BarEntry(10f, 3));
        entries_char2.add(new BarEntry(10f, 4));
        entries_char2.add(new BarEntry(7f, 5));
        entries_char2.add(new BarEntry(5f, 6));
        entries_char2.add(new BarEntry(5f, 7));
        entries_char2.add(new BarEntry(16f, 9));
    }

    private void AddValuesToPieEntryLabelsChar2() {
        PieEntryLabels_char2.add("Mahakam Take Over");
        PieEntryLabels_char2.add("Strategic Plan 2017");
        PieEntryLabels_char2.add("GRR Tuban Project");
        PieEntryLabels_char2.add("Geothermal Project");
        PieEntryLabels_char2.add("RUU Migas");
        PieEntryLabels_char2.add("International M&A");
        PieEntryLabels_char2.add("BUMN Holding");
        PieEntryLabels_char2.add("New & Renewable Energy");
        PieEntryLabels_char2.add("BUMN Report 2016");
    }

    private void AddValuesToPIEENTRYCHAR3() {
        entries_char3.add(new BarEntry(14f, 0));
        entries_char3.add(new BarEntry(14f, 1));
        entries_char3.add(new BarEntry(14f, 2));
        entries_char3.add(new BarEntry(9f, 3));
        entries_char3.add(new BarEntry(9f, 4));
        entries_char3.add(new BarEntry(7f, 5));
        entries_char3.add(new BarEntry(7f, 6));
        entries_char3.add(new BarEntry(7f, 7));
        entries_char3.add(new BarEntry(18f, 9));
    }

    private void AddValuesToPieEntryLabelsChar3() {
        PieEntryLabels_char3.add("Achandra Tahar");
        PieEntryLabels_char3.add("Rini Mariani Soernarno");
        PieEntryLabels_char3.add("Ignasius Jonan");
        PieEntryLabels_char3.add("Darmin Nasution");
        PieEntryLabels_char3.add("Sri Mulyani Indrawati");
        PieEntryLabels_char3.add("Joko Widodo");
        PieEntryLabels_char3.add("Sofyan Djalil");
        PieEntryLabels_char3.add("Luhut Binsar Panjaitan");
        PieEntryLabels_char3.add("Other");
    }

    private void AddValuesToPIEENTRYCHAR4() {
        entries_char4.add(new BarEntry(14f, 0));
        entries_char4.add(new BarEntry(14f, 1));
        entries_char4.add(new BarEntry(14f, 2));
        entries_char4.add(new BarEntry(9f, 3));
        entries_char4.add(new BarEntry(9f, 4));
        entries_char4.add(new BarEntry(7f, 5));
        entries_char4.add(new BarEntry(7f, 6));
        entries_char4.add(new BarEntry(7f, 7));
    }

    private void AddValuesToPieEntryLabelsChar4() {
        PieEntryLabels_char4.add("Capex (incl. M&A)");
        PieEntryLabels_char4.add("Financial performance");
        PieEntryLabels_char4.add("Financing/Funding Plan");
        PieEntryLabels_char4.add("Fuel Price/Policy");
        PieEntryLabels_char4.add("Mahakam");
        PieEntryLabels_char4.add("Oil and Gas holding");
        PieEntryLabels_char4.add("Pertagas");
        PieEntryLabels_char4.add("Refinery");
    }

    private void callFragment(Fragment fragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();
    }

    private void listLegendChart1(){
        LegendList l = new LegendList();
        l.setName_legend("Ministry of BUMN");
        l.setIcon_list(getResources().getColor(R.color.chartlv1));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Ministry of ESDM");
        l.setIcon_list(getResources().getColor(R.color.chartlv2));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("DPR");
        l.setIcon_list(getResources().getColor(R.color.chartlv3));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("MPR");
        l.setIcon_list(getResources().getColor(R.color.chartlv4));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("President of RI");
        l.setIcon_list(getResources().getColor(R.color.chartlv5));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Vice President of RI");
        l.setIcon_list(getResources().getColor(R.color.chartlv6));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Saudi Aramco");
        l.setIcon_list(getResources().getColor(R.color.chartlv7));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Total E & P");
        l.setIcon_list(getResources().getColor(R.color.chartlv8));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Others");
        l.setIcon_list(getResources().getColor(R.color.chartlv9));
        LegendsList.add(l);
    }

    private void listLegendChart2(){
        LegendList l = new LegendList();
        l.setName_legend("Mahakam Take Over");
        l.setIcon_list(getResources().getColor(R.color.chartlv1));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Strategic Plan 2017");
        l.setIcon_list(getResources().getColor(R.color.chartlv2));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("GRR Tuban Project");
        l.setIcon_list(getResources().getColor(R.color.chartlv3));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Geothermal Project");
        l.setIcon_list(getResources().getColor(R.color.chartlv4));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("RUU Migas");
        l.setIcon_list(getResources().getColor(R.color.chartlv5));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("International M&A");
        l.setIcon_list(getResources().getColor(R.color.chartlv6));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("BUMN Holding");
        l.setIcon_list(getResources().getColor(R.color.chartlv7));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("New & Renewable Energy");
        l.setIcon_list(getResources().getColor(R.color.chartlv8));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("BUMN Report 2016");
        l.setIcon_list(getResources().getColor(R.color.chartlv9));
        LegendsList2.add(l);
    }

    private void listLegendChart3(){
        LegendList l = new LegendList();
        l.setName_legend("Archandra Tahar");
        l.setIcon_list(getResources().getColor(R.color.chartlv1));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("Joko Widodo");
        l.setIcon_list(getResources().getColor(R.color.chartlv2));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("Rini Mariani Soemarno");
        l.setIcon_list(getResources().getColor(R.color.chartlv3));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("Sofyan Djalil");
        l.setIcon_list(getResources().getColor(R.color.chartlv4));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("Ignasius Jonan");
        l.setIcon_list(getResources().getColor(R.color.chartlv5));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("Luhut Binsar Panjaitan");
        l.setIcon_list(getResources().getColor(R.color.chartlv6));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("Darmin Nasution");
        l.setIcon_list(getResources().getColor(R.color.chartlv7));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("Others");
        l.setIcon_list(getResources().getColor(R.color.chartlv8));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("Sri Mulyani Indarwati");
        l.setIcon_list(getResources().getColor(R.color.chartlv9));
        LegendsList3.add(l);
    }

    private void listLegendChart4(){
        LegendList l = new LegendList();
        l.setName_legend("Capex (incl. M&A");
        l.setIcon_list(getResources().getColor(R.color.chartlv2));
        LegendsList4.add(l);

        l = new LegendList();
        l.setName_legend("Oil and Gas Holding");
        l.setIcon_list(getResources().getColor(R.color.chartlv3));
        LegendsList4.add(l);

        l = new LegendList();
        l.setName_legend("Financial performance");
        l.setIcon_list(getResources().getColor(R.color.chartlv4));
        LegendsList4.add(l);

        l = new LegendList();
        l.setName_legend("Pertagas");
        l.setIcon_list(getResources().getColor(R.color.chartlv5));
        LegendsList4.add(l);

        l = new LegendList();
        l.setName_legend("Financing/Funding Plan");
        l.setIcon_list(getResources().getColor(R.color.chartlv6));
        LegendsList4.add(l);

        l = new LegendList();
        l.setName_legend("Refinery");
        l.setIcon_list(getResources().getColor(R.color.chartlv7));
        LegendsList4.add(l);

        l = new LegendList();
        l.setName_legend("Fuel Price Policy");
        l.setIcon_list(getResources().getColor(R.color.chartlv8));
        LegendsList4.add(l);

        l = new LegendList();
        l.setName_legend("Mahakam");
        l.setIcon_list(getResources().getColor(R.color.chartlv9));
        LegendsList4.add(l);
    }
}
