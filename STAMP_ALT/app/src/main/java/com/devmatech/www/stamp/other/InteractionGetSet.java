package com.devmatech.www.stamp.other;

/**
 * Created by Dimas on 5/30/2017.
 */

public class InteractionGetSet {
    private String date_report, name_report, content_report;

    public InteractionGetSet(){}

    public InteractionGetSet(String date_report, String name_report, String content_report){
        this.date_report = date_report;
        this.name_report = name_report;
        this.content_report = content_report;
    }

    public String getDate_report(){
        return date_report;
    }

    public String getName_report() {
        return name_report;
    }

    public String getContent_report() {
        return content_report;
    }

    public void setDate_report(String date_report) {
        this.date_report = date_report;
    }

    public void setName_report(String name_report) {
        this.name_report = name_report;
    }

    public void setContent_report(String content_report) {
        this.content_report = content_report;
    }

}
