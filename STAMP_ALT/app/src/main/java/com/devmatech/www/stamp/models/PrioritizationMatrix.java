package com.devmatech.www.stamp.models;

import java.io.Serializable;

/**
 * Created by ASUS-PC on 16/06/2017.
 */

public class PrioritizationMatrix implements Serializable {

    private String id;

    private String col;

    private String q2;

    private String q1;

    private String q4;

    private String q3;

    private String name;

    private String row;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCol ()
    {
        return col;
    }

    public void setCol (String col)
    {
        this.col = col;
    }

    public String getQ2 ()
    {
        return q2;
    }

    public void setQ2 (String q2)
    {
        this.q2 = q2;
    }

    public String getQ1 ()
    {
        return q1;
    }

    public void setQ1 (String q1)
    {
        this.q1 = q1;
    }

    public String getQ4 ()
    {
        return q4;
    }

    public void setQ4 (String q4)
    {
        this.q4 = q4;
    }

    public String getQ3 ()
    {
        return q3;
    }

    public void setQ3 (String q3)
    {
        this.q3 = q3;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getRow ()
    {
        return row;
    }

    public void setRow (String row)
    {
        this.row = row;
    }
}
