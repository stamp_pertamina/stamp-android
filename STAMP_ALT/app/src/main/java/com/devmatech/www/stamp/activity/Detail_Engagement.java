package com.devmatech.www.stamp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.devmatech.www.stamp.R;

public class Detail_Engagement extends AppCompatActivity {

    TextView stitles, sub_title1, sub_title2, sub_title3, sub_title4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intn = getIntent();

        setContentView(R.layout.activity_detail_engagement);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        stitles = (TextView)findViewById(R.id.textView1);
        sub_title1 = (TextView)findViewById(R.id.textView2);
        sub_title2 = (TextView)findViewById(R.id.textView5);
        sub_title3 = (TextView)findViewById(R.id.textView10);
        sub_title4 = (TextView)findViewById(R.id.textView15);

        String id = intn.getStringExtra("id");
        if(id.toString().equals("c1"))
        {
            stitles.setText("Key Organization Engaged");
            sub_title1.setText("Ministry of BUMN");
            sub_title2.setText("Ministry of ESDM");
            sub_title3.setText("DPR");
            sub_title4.setText("Presiden of RI");
        }else
        if(id.toString().equals("c2"))
        {
            stitles.setText("Key Engagement Topics");
            sub_title1.setText("Mahakam Take Over");
            sub_title2.setText("GRR Tuban Project");
            sub_title3.setText("RUU Migas");
            sub_title4.setText("International M&A");
        }else
        if(id.toString().equals("c3"))
        {
            stitles.setText("Key Individual Engaged");
            sub_title1.setText("Ignasius Jonan");
            sub_title2.setText("Achandra Tahar");
            sub_title3.setText("Rini Mariani Suemarno");
            sub_title4.setText("Joko Widodo");
        }else
        if(id.toString().equals("c4"))
        {
            stitles.setText("Key Areas of Concern");
            sub_title1.setText("Capex (incl. M&A)");
            sub_title2.setText("Oil & Gas Holding");
            sub_title3.setText("Funding Plan");
            sub_title4.setText("Mahakam Take Over");
        }


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
