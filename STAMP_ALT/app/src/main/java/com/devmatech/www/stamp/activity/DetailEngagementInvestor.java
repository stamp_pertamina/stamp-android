package com.devmatech.www.stamp.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.devmatech.www.stamp.R;

public class DetailEngagementInvestor extends AppCompatActivity {
    TextView stitles, sub_title1, sub_title2, sub_title3, sub_title4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_engagement_investor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intn = getIntent();

        stitles = (TextView)findViewById(R.id.textView1);
        sub_title1 = (TextView)findViewById(R.id.textView2);
        sub_title2 = (TextView)findViewById(R.id.textView5);
        sub_title3 = (TextView)findViewById(R.id.textView10);
        sub_title4 = (TextView)findViewById(R.id.textView15);

        String id = intn.getStringExtra("id");
        if(id.toString().equals("c1"))
        {
            stitles.setText("Key Investors Engaged");
            sub_title1.setText("JP Morgan");
            sub_title2.setText("HSBC");
            sub_title3.setText("Hongkong Bank");
            sub_title4.setText("Bank Mandiri");
        }else
        if(id.toString().equals("c2"))
        {
            stitles.setText("Key Areas of Concern");
            sub_title1.setText("Mahakam Take Over");
            sub_title2.setText("GRR Tuban Project");
            sub_title3.setText("RUU Migas");
            sub_title4.setText("International M&A");
        }else
        if(id.toString().equals("c3"))
        {
            stitles.setText("Key Individual Engaged");
            sub_title1.setText("Ignasius Jonan");
            sub_title2.setText("Achandra Tahar");
            sub_title3.setText("Rini Mariani Suemarno");
            sub_title4.setText("Joko Widodo");
        }else
        if(id.toString().equals("c4"))
        {
            stitles.setText("Key Areas of Concern");
            sub_title1.setText("Capex (incl. M&A)");
            sub_title2.setText("Oil & Gas Holding");
            sub_title3.setText("Funding Plan");
            sub_title4.setText("Mahakam Take Over");
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
