
package com.devmatech.www.stamp.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OPModel implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("stamp_node")
    @Expose
    private String stampNode;
    @SerializedName("organization_type")
    @Expose
    private String organizationType;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("stakeholder_id")
    @Expose
    private String stakeholderId;
    @SerializedName("stakeholder_name")
    @Expose
    private String stakeholderName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("call_center")
    @Expose
    private String callCenter;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("key_individual")
    @Expose
    private List<KeyIndividual> keyIndividual = null;
    @SerializedName("prioritized_engagement_timeline")
    @Expose
    private List<PrioritizedEngagementTimeline> prioritizedEngagementTimeline = null;
    @SerializedName("source_1")
    @Expose
    private String source1;
    @SerializedName("source_2")
    @Expose
    private String source2;
    @SerializedName("last_update")
    @Expose
    private String lastUpdate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStampNode() { return stampNode; }

    public void setStampNode(String stampNode) { this.stampNode = stampNode; }

    public String getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(String organizationType) {
        this.organizationType = organizationType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStakeholderId() {
        return stakeholderId;
    }

    public void setStakeholderId(String stakeholderId) {
        this.stakeholderId = stakeholderId;
    }

    public String getStakeholderName() {
        return stakeholderName;
    }

    public void setStakeholderName(String stakeholderName) {
        this.stakeholderName = stakeholderName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCallCenter() {
        return callCenter;
    }

    public void setCallCenter(String callCenter) {
        this.callCenter = callCenter;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public List<KeyIndividual> getKeyIndividual() {
        return keyIndividual;
    }

    public void setKeyIndividual(List<KeyIndividual> keyIndividual) {
        this.keyIndividual = keyIndividual;
    }

    public List<PrioritizedEngagementTimeline> getPrioritizedEngagementTimeline() {
        return prioritizedEngagementTimeline;
    }

    public void setPrioritizedEngagementTimeline(List<PrioritizedEngagementTimeline> prioritizedEngagementTimeline) {
        this.prioritizedEngagementTimeline = prioritizedEngagementTimeline;
    }

    public String getSource1() {
        return source1;
    }

    public void setSource1(String source1) {
        this.source1 = source1;
    }

    public String getSource2() {
        return source2;
    }

    public void setSource2(String source2) {
        this.source2 = source2;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
