
package com.devmatech.www.stamp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListAreaOfConcern implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("concern")
    @Expose
    private String concern;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConcern() {
        return concern;
    }

    public void setConcern(String concern) {
        this.concern = concern;
    }

}
