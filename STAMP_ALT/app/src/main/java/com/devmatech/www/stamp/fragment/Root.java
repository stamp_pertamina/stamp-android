package com.devmatech.www.stamp.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.adapter.CustomAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Whelly on 30/05/2017.
 */

public class Root extends Fragment {

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Fragment fragment = null;
//    public GridView gridview;
    String [] listviewTitle = new String[]{"Strategic Objectives", "Organizations Profile", "Individuals Profile", "Engagement Summary", "Create Interaction Report"};
    int[] listviewImage = new int[]{R.drawable.ic_strategic, R.drawable.ic_organization, R.drawable.ic_individual, R.drawable.ic_engagement,
            R.drawable.ic_create};
    public Root(){}
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.root, container, false);
        List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < 5; i++) {
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("listview_title", listviewTitle[i]);
            hm.put("listview_image", Integer.toString(listviewImage[i]));
            aList.add(hm);
        }
        String[] from = {"listview_image", "listview_title", "listview_discription"};
        int[] to = {R.id.listview_image, R.id.listview_item_title};

        SimpleAdapter simpleAdapter = new SimpleAdapter(getActivity(), aList, R.layout.listview_root, from, to){
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the current item from ListView
                View view = super.getView(position,convertView,parent);
                if(position % 2 == 1)
                {
                    // Set a background color for ListView regular row/item
                    view.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
                else
                {
                    if (position == 2) {
                        // Set the background color for alternate row/item
                        view.setBackground(getResources().getDrawable(R.mipmap.pattern_menu_down));
                    } else {
                        view.setBackground(getResources().getDrawable(R.mipmap.pattern_menu_up));
                    }

                }
                return view;
            }
        };

        ListView androidListView = (ListView) rootView.findViewById(R.id.list_view);

        androidListView.setAdapter(simpleAdapter);
        androidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                fragmentManager = getFragmentManager();
                if(listviewTitle[position]== "Strategic Objectives"){
                    fragment = new Strategic();
                    callFragment(fragment);
                }else if(listviewTitle[position]== "Organizations Profile"){
                    fragment = new Organizations();
                    callFragment(fragment);
                }else if(listviewTitle[position]== "Individuals Profile"){
                    fragment = new Individuals();
                    callFragment(fragment);
                }else if(listviewTitle[position]== "Engagement Summary"){
                    fragment = new Engagement();
                    callFragment(fragment);
                }else if(listviewTitle[position]== "Create Interaction Report"){
                    fragment = new Interaction();
                    callFragment(fragment);
                }
            }
        });

//        gridview=(GridView) rootView.findViewById(R.id.gridView);
//        // setting up Adapter tp GridView
//        gridview.setAdapter(new CustomAdapter(getActivity(),app_name,app_icon));
//        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                fragmentManager = getFragmentManager();
//                if(app_name[position]== "Strategic Objectives"){
//                    //Toast.makeText(getActivity(), app_name[position], Toast.LENGTH_SHORT).show();
//
//                    fragment = new Strategic();
//                    callFragment(fragment);
//                }else if(app_name[position]== "Organizations Profile"){
//                    fragment = new Organizations();
//                    callFragment(fragment);
//                }else if(app_name[position]== "Individuals Profile"){
//                    fragment = new Individuals();
//                    callFragment(fragment);
//                }else if(app_name[position]== "Engagement Summary"){
//                    fragment = new Engagement();
//                    callFragment(fragment);
//                }else if(app_name[position]== "Create Interaction Report"){
//                    fragment = new Interaction();
//                    callFragment(fragment);
//                }
//            }
//        });

        getActivity().setTitle("Home");
        return rootView;
    }
    private void callFragment(Fragment fragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();
    }
}
