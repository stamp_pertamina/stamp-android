package com.devmatech.www.stamp.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.devmatech.www.stamp.R;

/**
 * Created by Whelly on 15/06/2017.
 */

public class StrategicAdapter extends ArrayAdapter<String> {
    private final Activity Context;
    private final String[] ListItemsName;
    private final String[] ListItemsDesc;
    private final Integer[] ImageName;

    public StrategicAdapter(Activity context, String[] content, String[] content_desc,
                       Integer[] ImageName) {

        super(context, R.layout.list_items_strategic_agenda, content);
        // TODO Auto-generated constructor stub

        this.Context = context;
        this.ListItemsName = content;
        this.ListItemsDesc = content_desc;
        this.ImageName = ImageName;
    }
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = Context.getLayoutInflater();
        View ListViewSingle = inflater.inflate(R.layout.list_items_strategic_agenda, null, true);

        TextView ListViewTitle = (TextView) ListViewSingle.findViewById(R.id.title);
        TextView ListViewDesc = (TextView) ListViewSingle.findViewById(R.id.desc);
        ImageView ListViewImage = (ImageView) ListViewSingle.findViewById(R.id.gambar);

        ListViewTitle.setText(ListItemsName[position]);
        ListViewDesc.setText(ListItemsDesc[position]);
        ListViewImage.setImageResource(ImageName[position]);
        return ListViewSingle;

    };
}
