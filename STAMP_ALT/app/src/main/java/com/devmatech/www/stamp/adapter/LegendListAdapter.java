package com.devmatech.www.stamp.adapter;

import android.content.Context;
import android.support.constraint.solver.widgets.WidgetContainer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.fragment.EngagementCsr;
import com.devmatech.www.stamp.models.IndividualsProfile;
import com.devmatech.www.stamp.models.LegendList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dimas on 6/14/2017.
 */

public class LegendListAdapter extends BaseAdapter {

    private ArrayList<LegendList> listLegend = new ArrayList<>();
    private Context context;
    private LayoutInflater mInflater;

    public LegendListAdapter(ArrayList<LegendList> listLegend, Context context) {
        this.listLegend = listLegend;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listLegend.size();
    }

    @Override
    public Object getItem(int position) {
        return listLegend.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListLegendViewHolder holder;
        int cellWidth = 40;
        int cellHeight = 50;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_legend, parent,false);
            holder = new ListLegendViewHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (ListLegendViewHolder) convertView.getTag();
        }

        final LegendList item = listLegend.get(position);
        holder.name.setText(item.getName_legend());
        holder.icon.setBackgroundColor(item.getIcon_list());

//        holder.name.setLayoutParams(new LinearLayout.LayoutParams(cellWidth, cellHeight));

//        holder.name.setPadding(1, 1, 1, 1);

        //
        return convertView;
    }


     class ListLegendViewHolder {
        public TextView name;
        public View icon;

        public ListLegendViewHolder(View view) {
            name = (TextView)view.findViewById(R.id.nameLegend);
            icon = view.findViewById(R.id.iconList);
        }
    }

}

