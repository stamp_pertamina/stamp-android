package com.devmatech.www.stamp.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.models.KeyIndividual;
import com.devmatech.www.stamp.models.OPModel;
import com.devmatech.www.stamp.models.Orgdetil;
import com.devmatech.www.stamp.models.PrioritizedEngagementTimeline;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URI;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.devmatech.www.stamp.R.id.ib_close;

public class DetailOrganization extends AppCompatActivity {

//    private final int numOfPages = 3; //viewpager has 4 pages
//    private final String[] pageTitle = {"Goals", "Description", "Agenda Pending"};
    ExpandableRelativeLayout expandableLayout1, expandableLayout2;
    ImageButton btn_set;
    String name;
    TableLayout tbl_prioritizedEngagementTimeline,tbl_keyIndividuals;
    TableRow tableRow;

    //----------tambahan---------------
    CircleImageView profile_image;
    //---------------------------------

    TextView txt_name, txt_address, txt_email, txt_callcenter, txt_website, txt_lastupdate, txt_kode, txt_moto, txt_description;
    private PopupWindow mPopupWindow;
    private Context mContext;
    private RelativeLayout mRelativeLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_organization);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = getApplicationContext();
        mRelativeLayout = (RelativeLayout) findViewById(R.id.layout_second);
        tbl_prioritizedEngagementTimeline = (TableLayout)findViewById(R.id.tbl_prioritizeEngagementTimeline);
        tbl_keyIndividuals = (TableLayout)findViewById(R.id.tbl_keyIndividuals); 
        btn_set = (ImageButton)findViewById(R.id.btn_img_appointment);
        txt_name = (TextView)findViewById(R.id.det_name);
        txt_lastupdate = (TextView)findViewById(R.id.det_lastUpdate);
        txt_kode = (TextView)findViewById(R.id.det_kode);
//        txt_moto = (TextView)findViewById(R.id.det_moto);
        txt_description = (TextView)findViewById(R.id.det_description);
        txt_address = (TextView)findViewById(R.id.det_address);
        txt_callcenter = (TextView)findViewById(R.id.det_phone);
        txt_email = (TextView)findViewById(R.id.det_email);
        txt_website = (TextView)findViewById(R.id.det_website);
        //-----tambahan--------
        profile_image = (CircleImageView)findViewById(R.id.profile_image);
        Intent i = getIntent();
        Bundle b = i.getExtras();
        OPModel prof = (OPModel) b.get("profile");
        name = prof.getStakeholderName();
        getSupportActionBar().setTitle(name);
        //--------------------

        Uri uri;
        if(prof.getId().length() > 0){
//            File file = new File(URI.create("file:///android_asset/OP/" + prof.getId()+".jpg"));
//            if (file.exists()){
                uri = Uri.parse("file:///android_asset/OP/" + prof.getId()+".jpg");
//            }else{
//                uri = Uri.parse("file:///android_asset/no_image.jpeg");
//            }
        }else {
            uri = Uri.parse("file:///android_asset/no_image.jpeg");
        }
        Picasso.with(this).load(uri).resize(100,100).centerCrop().into(profile_image);
        txt_name.setText(name);
        txt_lastupdate.setText("Last update: "+prof.getLastUpdate());
        txt_kode.setText(prof.getStampNode());
//        txt_moto.setText("Responsible for managing business in energy and mineral recources industry");
        txt_description.setText(prof.getDescription());
        txt_address.setText(prof.getAddress());
        txt_callcenter.setText(prof.getCallCenter());
        txt_email.setText(prof.getEmail());
        txt_website.setText(prof.getWebsite());

        btn_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);

                // Inflate the custom layout/view
                View customView = inflater.inflate(R.layout.dtl_org,null);
                // Initialize a new instance of popup window
                mPopupWindow = new PopupWindow(
                        customView,
                        ViewPager.LayoutParams.MATCH_PARENT,
                        ViewPager.LayoutParams.MATCH_PARENT
                );

                // Set an elevation value for popup window
                // Call requires API level 21
                if(Build.VERSION.SDK_INT>=21){
                    mPopupWindow.setElevation(5.0f);
                }

                // Get a reference for the custom view close button
                ImageButton closeButton = (ImageButton) customView.findViewById( ib_close);

                // Set a click listener for the popup window close button
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override

                    public void onClick(View view) {
                        // Dismiss the popup window
                        mPopupWindow.dismiss();

                    }
                });
                mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);
            }
        });

        for (int j = 0; j < prof.getPrioritizedEngagementTimeline().size(); j++) {
            PrioritizedEngagementTimeline pet = prof.getPrioritizedEngagementTimeline().get(j);
            tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
            tableRow.setWeightSum(1);
            TextView no = new TextView(this);
            no.setText(String.valueOf(j+1));
            no.setGravity(Gravity.LEFT);
            no.setPadding(5,5,5,5);
            no.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(no);
            TextView engagementType = new TextView(this);
            engagementType.setText(pet.getEngagementType());
            engagementType.setGravity(Gravity.LEFT);
            engagementType.setPadding(5,5,5,5);
            engagementType.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(engagementType);
            TextView date = new TextView(this);
            date.setText(pet.getEngagementDate());
            date.setGravity(Gravity.LEFT);
            date.setPadding(5,5,5,5);
            date.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(date);
            TextView topic = new TextView(this);
            topic.setText(pet.getEngagementTopic());
            topic.setGravity(Gravity.LEFT);
            topic.setPadding(5,5,5,5);
            topic.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(topic);
            tbl_prioritizedEngagementTimeline.addView(tableRow);
        }

        for (int j = 0; j < prof.getKeyIndividual().size(); j++) {
            KeyIndividual key = prof.getKeyIndividual().get(j);
            tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
            tableRow.setWeightSum(1);
            TextView individualProfile = new TextView(this);
            individualProfile.setText(key.getName());
            individualProfile.setGravity(Gravity.LEFT);
            individualProfile.setPadding(5,5,5,5);
            individualProfile.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(individualProfile);
            TextView title = new TextView(this);
            title.setText(key.getTitle());
            title.setGravity(Gravity.LEFT);
            title.setPadding(5,5,5,5);
            title.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(title);
            TextView department = new TextView(this);
            department.setText(key.getDepartment());
            department.setGravity(Gravity.LEFT);
            department.setPadding(5,5,5,5);
            department.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(department);
            TextView level = new TextView(this);
            level.setText(key.getLevel());
            level.setGravity(Gravity.LEFT);
            level.setPadding(5,5,5,5);
            level.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(level);
            tbl_keyIndividuals.addView(tableRow);
        }
    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager pager) {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void expandableButton1(View view) {
        expandableLayout1 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout1);
        expandableLayout1.toggle(); // toggle expand and collapse
    }

    public void expandableButton2(View view) {
        expandableLayout2 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout2);
        expandableLayout2.toggle(); // toggle expand and collapse
    }
}
