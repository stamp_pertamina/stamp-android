package com.devmatech.www.stamp.models;

import java.io.Serializable;

/**
 * Created by VILLA-PC on 6/1/2017.
 */

public class IndividualsProfile implements Serializable {
//    int id_;
    String id_, name,birth_date,address,phone,email,spouse_name,children_name,personal_interest,education,
    achievement,work_experience,political_aff,other_org_aff,personal_pref,jabatan,path_image,stats,last_update,
    kode,company,description;

    public String getId_() {
        return id_;
    }

    public void setId_(String id_) {
        this.id_ = id_;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getChildren_name() {
        return children_name;
    }

    public void setChildren_name(String children_name) {
        this.children_name = children_name;
    }

    public String getPersonal_interest() {
        return personal_interest;
    }

    public void setPersonal_interest(String personal_interest) {
        this.personal_interest = personal_interest;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getAchievement() {
        return achievement;
    }

    public void setAchievement(String achievement) {
        this.achievement = achievement;
    }

    public String getWork_experience() {
        return work_experience;
    }

    public void setWork_experience(String work_experience) {
        this.work_experience = work_experience;
    }

    public String getPolitical_aff() {
        return political_aff;
    }

    public void setPolitical_aff(String political_aff) {
        this.political_aff = political_aff;
    }

    public String getOther_org_aff() {
        return other_org_aff;
    }

    public void setOther_org_aff(String other_org_aff) {
        this.other_org_aff = other_org_aff;
    }

    public String getPersonal_pref() {
        return personal_pref;
    }

    public void setPersonal_pref(String personal_pref) {
        this.personal_pref = personal_pref;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getStats() { return stats; }

    public void setStats(String stats) { this.stats = stats; }

    public String getLast_update() { return last_update; }

    public void setLast_update(String last_update) { this.last_update = last_update; }

    public String getKode() { return kode;     }

    public void setKode(String kode) { this.kode = kode; }

    public String getCompany() { return company; }

    public void setCompany(String company) { this.company = company; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }
}
