package com.devmatech.www.stamp.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TableRow;
import android.widget.Toast;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.adapter.LegendListAdapter;
import com.devmatech.www.stamp.models.LegendList;
import com.devmatech.www.stamp.other.Remover;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.devmatech.www.stamp.R.id.tableRow1;
import static com.devmatech.www.stamp.R.id.tableRow2;
import static com.devmatech.www.stamp.R.id.tableRow3;
import static com.devmatech.www.stamp.R.id.tableRow4;
import static com.devmatech.www.stamp.R.id.tableRow5;

public class EngagementInvestor extends Fragment{

    ExpandableRelativeLayout expandableLayout;
    private GridView gridView,gridView2;

    ArrayList<LegendList> LegendsList = new ArrayList<>();
    ArrayList<LegendList> LegendsList2 = new ArrayList<>();

    LegendListAdapter adapterLegend,adapterLegend2;
    
    FragmentManager fragmentManager;
    Fragment fragment = null;
    FragmentTransaction fragmentTransaction;

    PieChart pieChart,pieChart2,pieChart3,pieChart4 ;
    ArrayList<Entry> entries,entries_char2,entries_char3,entries_char4 ;
    List dataSets;
    ArrayList<String> PieEntryLabels,PieEntryLabels_char2,PieEntryLabels_char3,PieEntryLabels_char4 ;
    PieDataSet pieDataSet,pieDataSetChar2,pieDataSetChar3,pieDataSetChar4 ;
    PieData pieData,pieDataChar2,pieDataChar3,pieDataChar4 ;
    BarDataSet barDataSet;
    private Context mContext;
    private PopupWindow mPopupWindow;
    private LinearLayout mRelativeLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.engagement_investor, container, false);

        gridView = (GridView) view.findViewById(R.id.grid_view);
        gridView2 = (GridView) view.findViewById(R.id.grid_view2);
        listLegendChart1();
        listLegendChart2();
        adapterLegend = new LegendListAdapter(LegendsList, getActivity().getBaseContext());
        adapterLegend2 = new LegendListAdapter(LegendsList2, getActivity().getBaseContext());
        gridView.setAdapter(adapterLegend);
        gridView2.setAdapter(adapterLegend2);

        mContext = getActivity();
        mRelativeLayout = (LinearLayout) view.findViewById(R.id.cover);

        //piechart 1
        pieChart = (PieChart) view.findViewById(R.id.chart1);
        pieChart.setHoleRadius(40f);
        pieChart.setDescription("");
        pieChart.setCenterText("View\nDetails");
        pieChart.setDrawCenterText(true);
        pieChart.setClickable(true);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
//                Log.i("I clicked ons", String.valueOf(e.getXIndex()) );
                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.DetailEngagementInvestor.class);
                myIntent.putExtra("id", "c1");
                startActivity(myIntent);
            }

            @Override
            public void onNothingSelected() {

            }
        });

        entries = new ArrayList<>();
        PieEntryLabels = new ArrayList<String>();
        AddValuesToPIEENTRY();
        AddValuesToPieEntryLabels();

        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new Remover(new DecimalFormat("###,###,###")));
        pieDataSet.setColors(new int[] {Color.rgb(0,108,183), Color.rgb(171,194,44), Color.rgb(237,27,46), Color.rgb(101,186,245), Color.rgb(146,152,111), Color.rgb(139,195,74), Color.rgb(34,149,136), Color.rgb(6,32,21), Color.rgb(180,180,180)});

        pieChart.setDrawSliceText(false);
        pieChart.setDescription("");
        pieChart.getLegend().setEnabled(false);

        pieData.setValueTextSize(12f);
        pieData.setValueTextColor(Color.rgb(255,255,255));

//            Legend legend = pieChart.getLegend();
//        legend.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
//        legend.setXEntrySpace(7f);
//        legend.setYEntrySpace(0f);
//        legend.setYOffset(0f);
//        legend.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
//        legend.setWordWrapEnabled(false);
        //legend.setEnabled(false);


        pieChart.setData(pieData);

        //pieChart.animateY(3000);

        //Pie Chart2
        pieChart2 = (PieChart) view.findViewById(R.id.chart2);
        pieChart2.setHoleRadius(40f);
        pieChart2.setDescription("");
        pieChart2.setCenterText("View\nDetails");
        pieChart2.setDrawCenterText(true);
        pieChart2.setClickable(true);
        pieChart2.setDrawHoleEnabled(true);
        pieChart2.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
//                Log.i("I clicked ons", String.valueOf(e.getXIndex()) );
                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.DetailEngagementInvestor.class);
                myIntent.putExtra("id", "c2");
                startActivity(myIntent);
            }

            @Override
            public void onNothingSelected() {

            }
        });

        entries_char2 = new ArrayList<>();
        PieEntryLabels_char2 = new ArrayList<>();
        AddValuesToPIEENTRYCHAR2();
        AddValuesToPieEntryLabelsChar2();
        pieDataSetChar2 = new PieDataSet(entries_char2, "");

        pieDataChar2 = new PieData(PieEntryLabels_char2, pieDataSetChar2);
        pieDataChar2.setValueFormatter(new Remover(new DecimalFormat("###,###,###")));
        pieDataSetChar2.setColors(new int[] {Color.rgb(0,108,183), Color.rgb(34,149,136), Color.rgb(171,194,44), Color.rgb(6,32,21), Color.rgb(237,27,46), Color.rgb(175,175,175), Color.rgb(101,186,245), Color.rgb(225,126,126), Color.rgb(146,152,111)});

        pieChart2.setDrawSliceText(false);
        pieChart2.setDescription("");
        pieChart2.getLegend().setEnabled(false);

        pieDataChar2.setValueTextSize(12f);
        pieDataChar2.setValueTextColor(Color.rgb(255,255,255));
//        Legend legendchar2 = pieChart2.getLegend();
//        legendchar2.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
//        legendchar2.setXEntrySpace(7f);
//        legendchar2.setYEntrySpace(0f);
//        legendchar2.setYOffset(0f);
//        legendchar2.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
//          legendchar2.setWordWrapEnabled(false);
//        legendchar2.setEnabled(false);


        pieChart2.setData(pieDataChar2);

        //pieChart2.animateY(3000);
        //----------------------------------------------------------------------------------------------//
        HorizontalBarChart barChart = (HorizontalBarChart) view.findViewById(R.id.chart);

        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(4f, 0));
        entries.add(new BarEntry(8f, 1));
        entries.add(new BarEntry(6f, 2));
        entries.add(new BarEntry(12f, 3));
        entries.add(new BarEntry(18f, 4));

        BarDataSet dataset = new BarDataSet(entries, "Frequencies");
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);

        ArrayList<String> labels = new ArrayList<String>();
                labels.add("Individual E");
                labels.add("Individual D");
                labels.add("Individual C");
                labels.add("Individual B");
                labels.add("Individual A");

        BarData data = new BarData(labels, dataset);
        barChart.setData(data);
        barChart.setDescription("");

        //----------------------------------------------------------------------------------------------//

        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.rgb(255, 156, 9), PorterDuff.Mode.SRC_ATOP);

        RatingBar ratingBar2 = (RatingBar) view.findViewById(R.id.ratingBar2);
        LayerDrawable stars2 = (LayerDrawable) ratingBar2.getProgressDrawable();
        stars2.getDrawable(2).setColorFilter(Color.rgb(255, 156, 9), PorterDuff.Mode.SRC_ATOP);

        RatingBar ratingBar3 = (RatingBar) view.findViewById(R.id.ratingBar3);
        LayerDrawable stars3 = (LayerDrawable) ratingBar3.getProgressDrawable();
        stars3.getDrawable(2).setColorFilter(Color.rgb(255, 156, 9), PorterDuff.Mode.SRC_ATOP);

        //---------------------------------------------------------------------------------------------

        Button b1 = (Button) view.findViewById(R.id.expandableButtonz);
        b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                expandableLayout = (ExpandableRelativeLayout) view.findViewById(R.id.expandableLayoutz);
                expandableLayout.toggle(); // toggle expand and collapse
            }
        });

        Button b2 = (Button) view.findViewById(R.id.expandableButtonz2);
        b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                expandableLayout = (ExpandableRelativeLayout) view.findViewById(R.id.expandableLayoutz2);
                expandableLayout.toggle(); // toggle expand and collapse
            }
        });

        Button b3 = (Button) view.findViewById(R.id.expandableButtonz3);
        b3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                expandableLayout = (ExpandableRelativeLayout) view.findViewById(R.id.expandableLayoutz3);
                expandableLayout.toggle(); // toggle expand and collapse
            }
        });

        Button b4 = (Button) view.findViewById(R.id.expandableButtonz4);
        b4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                expandableLayout = (ExpandableRelativeLayout) view.findViewById(R.id.expandableLayoutz4);
                expandableLayout.toggle(); // toggle expand and collapse
            }
        });

        //---------------------------------------------------------------------------------------------
        final BottomNavigationView bottomNavigationView = (BottomNavigationView) view.findViewById( R.id.bottomMenuEngagement );

        bottomNavigationView.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener(){

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()){
                    case R.id.menu_all:

//                        PopupMenu popup = new PopupMenu(getActivity(),  bottomNavigationView);
//                        // Inflating menu using xml file
//                        popup.getMenuInflater().inflate(R.menu.all_scf_menu, popup.getMenu());
//
//                        // registering OnMenuItemClickListener
//                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                fragmentManager = getFragmentManager();
//                                switch (item.getTitle().toString()){
//                                    case "Institutional Relation" :
//                                        fragment = new Engagement();
//                                        callFragment(fragment);
//                                        break;
//                                    case "Investor Relation" :
//                                        fragment = new EngagementInvestor();
//                                        callFragment(fragment);
//                                        break;
//                                    case "International" :
//                                        fragment = new EngagementInternational();
//                                        callFragment(fragment);
//                                        break;
//                                    case "Media" :
//                                        fragment = new EngagementMedia();
//                                        callFragment(fragment);
//                                        break;
//                                    case "CSR" :
//                                        fragment = new EngagementCsr();
//                                        callFragment(fragment);
//                                        break;
//                                }
//                                return true;
//                            }
//                        });
//                        popup.show();

                        LayoutInflater inflater2 = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
//
                        // Inflate the custom layout/view
                        View customView2 = inflater2.inflate(R.layout.all_menu_eng,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView2,
                                ViewPager.LayoutParams.MATCH_PARENT,
                                ViewPager.LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button
                        fragmentManager = getFragmentManager();
                        final TableRow tes1 = (TableRow) customView2.findViewById(tableRow1) ;
                        final TableRow tes2 = (TableRow) customView2.findViewById(tableRow2) ;
                        final TableRow tes3 = (TableRow) customView2.findViewById(tableRow3) ;
                        final TableRow tes4 = (TableRow) customView2.findViewById(tableRow4) ;
                        final TableRow tes5 = (TableRow) customView2.findViewById(tableRow5) ;

                        // Set a click listener for the popup window close button
                        tes1.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                fragment = new Engagement();
                                callFragment(fragment);
                                mPopupWindow.dismiss();

                            }
                        });tes2.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            fragment = new EngagementInvestor();
                            callFragment(fragment);
                            mPopupWindow.dismiss();

                        }
                    });tes3.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            fragment = new EngagementInternational();
                            callFragment(fragment);
                            mPopupWindow.dismiss();

                        }
                    });tes4.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            fragment = new EngagementMedia();
                            callFragment(fragment);
                            mPopupWindow.dismiss();

                        }
                    });tes5.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            fragment = new EngagementCsr();
                            callFragment(fragment);
                            mPopupWindow.dismiss();

                        }
                    });



                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);

                        break;
                    case R.id.menu_strategic:
//                        PopupMenu popup_strategic = new PopupMenu(getActivity(),  bottomNavigationView);
//                        // Inflating menu using xml file
//                        popup_strategic.getMenuInflater().inflate(R.menu.strategic_objectives_menu, popup_strategic.getMenu());
//
//                        // registering OnMenuItemClickListener
//                        popup_strategic.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                Toast.makeText(getActivity(),
//                                        "Kamu telah memilih : " + item.getTitle(),
//                                        Toast.LENGTH_SHORT).show();
//                                return true;
//                            }
//                        });
//                        popup_strategic.show();

                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
//
                        // Inflate the custom layout/view
                        View customView = inflater.inflate(R.layout.str_menu_eng,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView,
                                ViewPager.LayoutParams.MATCH_PARENT,
                                ViewPager.LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button
                        final TableRow tes_1 = (TableRow) customView.findViewById(tableRow1) ;
                        final TableRow tes_2 = (TableRow) customView.findViewById(tableRow2) ;

                        // Set a click listener for the popup window close button
                        tes_1.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                Toast.makeText(getActivity(),
                                        "Kamu telah memilih : Take Over Indonesia Blocks",
                                        Toast.LENGTH_SHORT).show();
                                mPopupWindow.dismiss();

                            }
                        });tes_2.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih : Grass Root Refinery(GRR) Tuban",
                                    Toast.LENGTH_SHORT).show();
                            mPopupWindow.dismiss();

                        }
                    });



                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);

                        break;
                    case R.id.menu_periode:
//                        PopupMenu popup_periode = new PopupMenu(getActivity(),  bottomNavigationView);
//                        // Inflating menu using xml file
//                        popup_periode.getMenuInflater().inflate(R.menu.engagement_periode_menu, popup_periode.getMenu());
//
//                        // registering OnMenuItemClickListener
//                        popup_periode.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                Toast.makeText(getActivity(),
//                                        "Kamu telah memilih : " + item.getTitle(),
//                                        Toast.LENGTH_SHORT).show();
//                                return true;
//                            }
//                        });
//                        popup_periode.show();

                        LayoutInflater inflater3 = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
//
                        // Inflate the custom layout/view
                        View customView3 = inflater3.inflate(R.layout.per_menu_eng,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView3,
                                ViewPager.LayoutParams.MATCH_PARENT,
                                ViewPager.LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button
                        final TableRow tes1_ = (TableRow) customView3.findViewById(tableRow1) ;
                        final TableRow tes2_ = (TableRow) customView3.findViewById(tableRow2) ;
                        final TableRow tes3_ = (TableRow) customView3.findViewById(tableRow3) ;

                        // Set a click listener for the popup window close button
                        tes1_.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                Toast.makeText(getActivity(),
                                        "Kamu telah memilih : 1 Apr 2017 - 30 Apr 2017",
                                        Toast.LENGTH_SHORT).show();
                                mPopupWindow.dismiss();

                            }
                        });tes2_.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih : 1 Mar 2017 - 31 Mar 2017",
                                    Toast.LENGTH_SHORT).show();
                            mPopupWindow.dismiss();

                        }
                    });tes3_.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih : 1 Feb 2017 - 28 Feb 2017",
                                    Toast.LENGTH_SHORT).show();
                            mPopupWindow.dismiss();

                        }
                    });



                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);

                        break;
                }
                return true;
            }
        } );

        return view;
    }



    public void AddValuesToPIEENTRY(){

        entries.add(new BarEntry(15f, 0));
        entries.add(new BarEntry(14f, 1));
        entries.add(new BarEntry(12f, 2));
        entries.add(new BarEntry(9f, 3));
        entries.add(new BarEntry(9f, 4));
        entries.add(new BarEntry(6f, 5));
        entries.add(new BarEntry(6f, 6));
        entries.add(new BarEntry(6f, 7));
        entries.add(new BarEntry(23f, 9));

    }

    public void AddValuesToPieEntryLabels(){

        PieEntryLabels.add("Ministry of BUMN");
        PieEntryLabels.add("Ministry of ESDM");
        PieEntryLabels.add("DPR");
        PieEntryLabels.add("MPR");
        PieEntryLabels.add("Presiden of RI");
        PieEntryLabels.add("Vice Presiden of RI");
        PieEntryLabels.add("Saudi Aramco");
        PieEntryLabels.add("Total E & P");
        PieEntryLabels.add("Other");
    }

    private void AddValuesToPIEENTRYCHAR2() {
        entries_char2.add(new BarEntry(16f, 0));
        entries_char2.add(new BarEntry(15f, 1));
        entries_char2.add(new BarEntry(11f, 2));
        entries_char2.add(new BarEntry(10f, 3));
        entries_char2.add(new BarEntry(10f, 4));
        entries_char2.add(new BarEntry(7f, 5));
        entries_char2.add(new BarEntry(5f, 6));
        entries_char2.add(new BarEntry(5f, 7));
        entries_char2.add(new BarEntry(16f, 9));
    }

    private void AddValuesToPieEntryLabelsChar2() {
        PieEntryLabels_char2.add("Mahakam Take Over");
        PieEntryLabels_char2.add("Strategic Plan 2017");
        PieEntryLabels_char2.add("GRR Tuban Project");
        PieEntryLabels_char2.add("Geothermal Project");
        PieEntryLabels_char2.add("RUU Migas");
        PieEntryLabels_char2.add("International M&A");
        PieEntryLabels_char2.add("BUMN Holding");
        PieEntryLabels_char2.add("New & Renewable Energy");
        PieEntryLabels_char2.add("BUMN Report 2016");
    }

    private void AddValuesToPIEENTRYCHAR3() {
        entries_char3.add(new BarEntry(14f, 0));
        entries_char3.add(new BarEntry(14f, 1));
        entries_char3.add(new BarEntry(14f, 2));
        entries_char3.add(new BarEntry(9f, 3));
        entries_char3.add(new BarEntry(9f, 4));
        entries_char3.add(new BarEntry(7f, 5));
        entries_char3.add(new BarEntry(7f, 6));
        entries_char3.add(new BarEntry(7f, 7));
        entries_char3.add(new BarEntry(18f, 9));
    }

    private void AddValuesToPieEntryLabelsChar3() {
        PieEntryLabels_char3.add("Achandra Tahar");
        PieEntryLabels_char3.add("Rini Mariani Soernarno");
        PieEntryLabels_char3.add("Ignasius Jonan");
        PieEntryLabels_char3.add("Darmin Nasution");
        PieEntryLabels_char3.add("Sri Mulyani Indrawati");
        PieEntryLabels_char3.add("Joko Widodo");
        PieEntryLabels_char3.add("Sofyan Djalil");
        PieEntryLabels_char3.add("Luhut Binsar Panjaitan");
        PieEntryLabels_char3.add("Other");
    }

    private void AddValuesToPIEENTRYCHAR4() {
        entries_char4.add(new BarEntry(14f, 0));
        entries_char4.add(new BarEntry(14f, 1));
        entries_char4.add(new BarEntry(14f, 2));
        entries_char4.add(new BarEntry(9f, 3));
        entries_char4.add(new BarEntry(9f, 4));
        entries_char4.add(new BarEntry(7f, 5));
        entries_char4.add(new BarEntry(7f, 6));
        entries_char4.add(new BarEntry(7f, 7));
    }

    private void AddValuesToPieEntryLabelsChar4() {
        PieEntryLabels_char4.add("Capex (incl. M&A)");
        PieEntryLabels_char4.add("Financial performance");
        PieEntryLabels_char4.add("Financing/Funding Plan");
        PieEntryLabels_char4.add("Fuel Price/Policy");
        PieEntryLabels_char4.add("Mahakam");
        PieEntryLabels_char4.add("Oil and Gas holding");
        PieEntryLabels_char4.add("Pertagas");
        PieEntryLabels_char4.add("Refinery");
    }

    private void callFragment(Fragment fragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();
    }

    private void listLegendChart1(){
        LegendList l = new LegendList();
        l.setName_legend("JP Morgan");
        l.setIcon_list(getResources().getColor(R.color.chartlv1));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("HSBC");
        l.setIcon_list(getResources().getColor(R.color.chartlv2));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Hongkong Bank");
        l.setIcon_list(getResources().getColor(R.color.chartlv3));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Bank Mandiri");
        l.setIcon_list(getResources().getColor(R.color.chartlv4));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Bank of China");
        l.setIcon_list(getResources().getColor(R.color.chartlv5));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Bank of Japan");
        l.setIcon_list(getResources().getColor(R.color.chartlv6));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("OSBC");
        l.setIcon_list(getResources().getColor(R.color.chartlv7));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Standard Chartered");
        l.setIcon_list(getResources().getColor(R.color.chartlv8));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Others");
        l.setIcon_list(getResources().getColor(R.color.chartlv9));
        LegendsList.add(l);
    }

    private void listLegendChart2(){
        LegendList l = new LegendList();
        l.setName_legend("Capex (incl. M&A");
        l.setIcon_list(getResources().getColor(R.color.chartlv2));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Oil and Gas Holding");
        l.setIcon_list(getResources().getColor(R.color.chartlv3));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Financial performance");
        l.setIcon_list(getResources().getColor(R.color.chartlv4));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Pertagas");
        l.setIcon_list(getResources().getColor(R.color.chartlv5));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Financing/Funding Plan");
        l.setIcon_list(getResources().getColor(R.color.chartlv6));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Refinery");
        l.setIcon_list(getResources().getColor(R.color.chartlv7));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Fuel Price Policy");
        l.setIcon_list(getResources().getColor(R.color.chartlv8));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Mahakam");
        l.setIcon_list(getResources().getColor(R.color.chartlv9));
        LegendsList2.add(l);
    }


}
