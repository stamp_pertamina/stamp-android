
package com.devmatech.www.stamp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LatestStakeholderPerception implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("strategic_objective")
    @Expose
    private String strategicObjective;
    @SerializedName("perception")
    @Expose
    private String perception;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStrategicObjective() {
        return strategicObjective;
    }

    public void setStrategicObjective(String strategicObjective) {
        this.strategicObjective = strategicObjective;
    }

    public String getPerception() {
        return perception;
    }

    public void setPerception(String perception) {
        this.perception = perception;
    }

}
