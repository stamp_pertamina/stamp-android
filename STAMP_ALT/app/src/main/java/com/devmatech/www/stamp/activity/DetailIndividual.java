package com.devmatech.www.stamp.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.models.EngagementRecord;
import com.devmatech.www.stamp.models.IPModel;
import com.devmatech.www.stamp.models.IndividualsProfile;
import com.devmatech.www.stamp.models.InternalPertaminaContact;
import com.devmatech.www.stamp.models.LatestStakeholderPerception;
import com.devmatech.www.stamp.models.ListAreaOfConcern;
import com.devmatech.www.stamp.models.StakeholderNetwork;
import com.devmatech.www.stamp.other.Helper;
import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;
import java.net.URI;

import de.hdodenhof.circleimageview.CircleImageView;

public class DetailIndividual extends AppCompatActivity{

    ExpandableRelativeLayout expandLayoutStake,expandLayoutInternal,expandLayoutLatest
            ,expandLayoutEngagement,expandLayoutList;
    CircleImageView profile_image;
    ImageButton btn_set,btn_news;
    TableLayout tbl_stakeholder,tbl_internal_pertamina_contact,tbl_latest_stakeholer_perception,
    tbl_engagement_record,tbl_list_of_area_concerns;
    TableRow tableRow;
    String name;
    TextView txt_name,txt_birthdate,txt_address,txt_phone,txt_email,txt_spouse,txt_child,txt_pers_inter,txt_edu
            ,txt_achievement,txt_work_exp,txt_politic,txt_other_org,txt_pers_pref,txt_type,txt_last_update,txt_code
            ,txt_company,txt_description;
    Typeface FontAwesome;
    Helper helper = new Helper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_individual);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FontAwesome = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf" );
        btn_set = (ImageButton)findViewById(R.id.btn_img_appointment);
        btn_news = (ImageButton)findViewById(R.id.btn_img_news);
        txt_name = (TextView)findViewById(R.id.det_name);
        txt_birthdate = (TextView)findViewById(R.id.det_birthDate);
        txt_address = (TextView)findViewById(R.id.det_address);
        txt_phone = (TextView)findViewById(R.id.det_phone);
        txt_email = (TextView)findViewById(R.id.det_email);
        txt_spouse = (TextView)findViewById(R.id.det_spouse);
        txt_child = (TextView)findViewById(R.id.det_children);
        txt_pers_inter = (TextView)findViewById(R.id.det_personalInterest);
        txt_edu = (TextView)findViewById(R.id.det_edu);
        txt_achievement = (TextView)findViewById(R.id.det_achievement);
        txt_work_exp = (TextView)findViewById(R.id.det_work);
        txt_politic = (TextView)findViewById(R.id.det_political);
        txt_other_org = (TextView)findViewById(R.id.det_otherOrg);
        txt_pers_pref = (TextView)findViewById(R.id.det_personalPref);
        txt_type = (TextView)findViewById(R.id.det_type);
        txt_last_update = (TextView)findViewById(R.id.det_lastUpdate);
        txt_code = (TextView)findViewById(R.id.det_kode);
        txt_company = (TextView)findViewById(R.id.det_company);
        txt_description = (TextView)findViewById(R.id.det_description);
        profile_image = (CircleImageView)findViewById(R.id.profile_image);
        tbl_stakeholder = (TableLayout)findViewById(R.id.tbl_stakeholder);
        tbl_internal_pertamina_contact = (TableLayout)findViewById(R.id.tbl_internalPertaminaContact);
        tbl_latest_stakeholer_perception = (TableLayout)findViewById(R.id.tbl_latestStakeholderPerception);
        tbl_engagement_record = (TableLayout)findViewById(R.id.tbl_engagementRecord);
        tbl_list_of_area_concerns = (TableLayout)findViewById(R.id.tbl_listAreaConcerns);
//        tbl_list_of_area_concerns.setStretchAllColumns(true);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        IPModel prof = (IPModel) b.get("profile");
        name = prof.getIndividualName();
        getSupportActionBar().setTitle(name);


        String childNames = getAttrName(prof.getChildrenName1())+getAttrName(prof.getChildrenName2())+getAttrName(prof.getChildrenName3())+getAttrName(prof.getChildrenName4())+
                getAttrName(prof.getChildrenName5())+getAttrName(prof.getChildrenName6())+getAttrName(prof.getChildrenName7())+getAttrName(prof.getChildrenName8())+
                getAttrName(prof.getChildrenName9())+getAttrName(prof.getChildrenName10());

        String education = getAttrName(prof.getEducationDetail1())+getAttrName(prof.getEducationDetail2())+getAttrName(prof.getEducationDetail3());
        String working = getAttrName(prof.getWorkingExperience1())+getAttrName(prof.getWorkingExperience2())+getAttrName(prof.getWorkingExperience3())+
                getAttrName(prof.getWorkingExperience4())+getAttrName(prof.getWorkingExperience5())+getAttrName(prof.getWorkingExperience6())+getAttrName(prof.getWorkingExperience7())+
                getAttrName(prof.getWorkingExperience8())+getAttrName(prof.getWorkingExperience9())+getAttrName(prof.getWorkingExperience10());
        String political = getAttrName(prof.getPoliticalAffiliation1())+getAttrName(prof.getPoliticalAffiliation2())+
                getAttrName(prof.getPoliticalAffiliation3())+getAttrName(prof.getPoliticalAffiliation4());
        String otherOrg = getAttrName(prof.getOtherOrganizationalAffiliation1())+getAttrName(prof.getOtherOrganizationalAffiliation2())+
                getAttrName(prof.getOtherOrganizationalAffiliation3())+getAttrName(prof.getOtherOrganizationalAffiliation4());

        Uri uri;
        if(prof.getId().length() > 0){
//            File file = new File(URI.create("file:///android_asset/IP/" + prof.getId()+".jpg"));
//            Log.e("path","path: "+file.getAbsolutePath());
//            if (file.exists()){
                uri = Uri.parse("file:///android_asset/IP/" + prof.getId()+".jpg");
//            }else{
//                uri = Uri.parse("file:///android_asset/placeholder-person.jpg");
//            }
        }else {
            uri = Uri.parse("file:///android_asset/placeholder-person.jpg");
        }
        Picasso.with(this).load(uri).resize(100,100).centerCrop().into(profile_image);
        txt_name.setText(name);
        txt_type.setText(prof.getType());
        txt_last_update.setText("Last Update: "+prof.getLastUpdate());
        txt_code.setText(prof.getStampNode());
        txt_company.setText(prof.getOrganizationName());
        txt_description.setText(prof.getTitleName());
        txt_birthdate.setText(prof.getBirthDate());
        txt_address.setText(prof.getAddress());
        txt_phone.setText(prof.getPhoneNumber());
        txt_email.setText(prof.getEmail());
        txt_spouse.setText(prof.getSpouseName());
        txt_child.setText(childNames);
        txt_pers_inter.setText(prof.getPersonalInterest());
        txt_edu.setText(education);
        txt_achievement.setText(prof.getAchievement());
        txt_work_exp.setText(working);
        txt_politic.setText(political);
        txt_other_org.setText(otherOrg);
        txt_pers_pref.setText(prof.getPersonalPreferences());

        btn_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getBaseContext(),FormSetAppointment.class);
                in.putExtra("name",name);
                startActivity(in);
            }
        });

        btn_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getBaseContext(),NewsActivity.class);
                in.putExtra("name",name);
                startActivity(in);
            }
        });

        for (int j = 0; j < prof.getStakeholderNetwork().size(); j++) {
            StakeholderNetwork sn = prof.getStakeholderNetwork().get(j);
            tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
            tableRow.setWeightSum(1);
            TextView st_name = new TextView(this);
            st_name.setText(sn.getName());
            st_name.setGravity(Gravity.LEFT);
            st_name.setPadding(20,30,20,30);
            st_name.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(st_name);
            TextView st_org = new TextView(this);
            st_org.setText(sn.getName());
            st_org.setGravity(Gravity.LEFT);
            st_org.setPadding(20,30,20,30);
            st_org.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(st_org);
            TextView st_email = new TextView(this);
            st_email.setText(sn.getName());
            st_email.setGravity(Gravity.LEFT);
            st_email.setPadding(20,30,20,30);
            st_email.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(st_email);
            TextView st_phone = new TextView(this);
            st_phone.setText(sn.getName());
            st_phone.setGravity(Gravity.LEFT);
            st_phone.setPadding(20,30,20,30);
            st_phone.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(st_phone);
            tbl_stakeholder.addView(tableRow);
        }
        for (int j = 0; j < prof.getInternalPertaminaContact().size(); j++) {
            InternalPertaminaContact ipc = prof.getInternalPertaminaContact().get(j);
            tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
            tableRow.setWeightSum(1);
            TextView name = new TextView(this);
            name.setText(ipc.getName());
            name.setGravity(Gravity.LEFT);
            name.setPadding(20,30,20,30);
            name.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(name);
            TextView division = new TextView(this);
            division.setText(ipc.getDevision());
            division.setGravity(Gravity.LEFT);
            division.setPadding(20,30,20,30);
            division.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(division);
            TextView email = new TextView(this);
            email.setText(ipc.getEmail());
            email.setGravity(Gravity.LEFT);
            email.setPadding(20,30,20,30);
            email.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(email);
            tbl_internal_pertamina_contact.addView(tableRow);
        }
        for (int j = 0; j < prof.getLatestStakeholderPerception().size(); j++) {
            LatestStakeholderPerception lsp = prof.getLatestStakeholderPerception().get(j);
            tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
            tableRow.setWeightSum(1);
            TextView no = new TextView(this);
            no.setText(String.valueOf(j+1));
            no.setGravity(Gravity.CENTER);
            no.setPadding(20,30,20,30);
            no.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(no);
            TextView SO = new TextView(this);
            SO.setText(lsp.getStrategicObjective());
            SO.setGravity(Gravity.LEFT);
            SO.setPadding(20,30,20,30);
            SO.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(SO);
            TextView perception = new TextView(this);
            perception.setTypeface(FontAwesome);
            perception.setText(helper.fromHtml(getPerceptionIcon(lsp.getPerception())));
            perception.setGravity(Gravity.CENTER);
            perception.setPadding(20,30,20,30);
            perception.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(perception);
            tbl_latest_stakeholer_perception.addView(tableRow);
        }
        for (int j = 0; j < prof.getEngagementRecord().size(); j++) {
            EngagementRecord er = prof.getEngagementRecord().get(j);
            tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
            tableRow.setWeightSum(1);
            TextView no = new TextView(this);
            no.setText(String.valueOf(j+1));
            no.setGravity(Gravity.CENTER);
            no.setPadding(20,30,20,30);
            no.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(no);
            TextView date = new TextView(this);
            date.setText(er.getDate());
            date.setGravity(Gravity.LEFT);
            date.setPadding(20,30,20,30);
            date.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(date);
            TextView topic = new TextView(this);
            topic.setText(er.getTopic());
            topic.setGravity(Gravity.LEFT);
            topic.setPadding(20,30,20,30);
            topic.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(topic);
            TextView engagementType = new TextView(this);
            engagementType.setText(er.getEngagementType());
            engagementType.setGravity(Gravity.LEFT);
            engagementType.setPadding(20,30,20,30);
            engagementType.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(engagementType);
            tbl_engagement_record.addView(tableRow);
        }
        for (int j = 0; j < prof.getListAreaOfConcern().size(); j++) {
            ListAreaOfConcern lac = prof.getListAreaOfConcern().get(j);
            tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
            tableRow.setWeightSum(1);
            TextView concern = new TextView(this);
            concern.setText(lac.getConcern());
            concern.setGravity(Gravity.CENTER);
            concern.setPadding(20,30,20,30);
            concern.setBackgroundResource(R.drawable.value_shape);
            tableRow.addView(concern);
            tbl_list_of_area_concerns.addView(tableRow);
        }
    }

    public String getAttrName(String names){
        if(names.length() == 0){
            return "";
        }else{
            return names+"\n";
        }
    }

    public String getPerceptionIcon(String perception){
        String result = "";
        if(!perception.isEmpty()){
            if (perception.equals("Supportive")){
                result = "&#xf10c;";
            }else if(perception.equals("Less Supportive")){
                result = "&#xf111;";
            }else if(perception.equals("Neutral")){
                result = "&#xf042;";
            }
        }
        return result;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void layoutStakeholder(View view){
        expandLayoutStake = (ExpandableRelativeLayout)findViewById(R.id.expandLayoutStakeholder);
        expandLayoutStake.toggle();
    }
    public void layoutInternal(View view){
        expandLayoutInternal = (ExpandableRelativeLayout)findViewById(R.id.expandLayoutInternal);
        expandLayoutInternal.toggle();
    }
    public void layoutLatest(View view){
        expandLayoutLatest = (ExpandableRelativeLayout)findViewById(R.id.expandLayoutLatest);
        expandLayoutLatest.toggle();
    }
    public void layoutEngagement(View view){
        expandLayoutEngagement = (ExpandableRelativeLayout)findViewById(R.id.expandLayoutEngagement);
        expandLayoutEngagement.toggle();
    }
    public void layoutList(View view){
        expandLayoutList = (ExpandableRelativeLayout)findViewById(R.id.expandLayoutList);
        expandLayoutList.toggle();
    }
}
