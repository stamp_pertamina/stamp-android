package com.devmatech.www.stamp.models;

/**
 * Created by ASUS-PC on 21/06/2017.
 */

public class InteractionDownload {
    String name,size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
