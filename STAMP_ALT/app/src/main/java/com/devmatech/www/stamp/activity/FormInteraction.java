package com.devmatech.www.stamp.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.other.Helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class FormInteraction extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    String[] list_item = {"Item 1", "Item 2", "Item3"};
    String[] list_stakeholer = {"Name 01", "Name 02", "Name 03","Name 04","Name 05"};
    String[] icon_degree = {"&#xf111;","&#xf042;","&#xf10c;"};
    String[] degree_of_support = {"Full Support","Half Support","Need More Support"};
    EditText et_date, et_deadline, et_list_stakeholder,et_list_stakeholder_area;
    Calendar calendar = Calendar.getInstance();
    Helper helper = new Helper();
    Button btn_simpan, btn_upload;
    Typeface FontAwesome;


    String[] list_area = {"Name 01", "Name 02", "Name 03","Name 04","Name 05"};
    int check = 0;

    private static final String VALUE_STRING = "Value %d";

    //private HintSpinner<String> defaultHintSpinner;
    private List<String> defaults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_interaction);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FontAwesome = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf" );

        btn_simpan = (Button) findViewById(R.id.buttonSubmit);
        btn_upload = (Button) findViewById(R.id.buttonUpload);

        //set combo box 1
        Spinner spin1 = (Spinner) findViewById(R.id.corporate);
        spin1.setOnItemSelectedListener(this);
        ArrayAdapter<String> aa1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list_item);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin1.setAdapter(aa1);

        //set combo box 2
        Spinner spin2 = (Spinner) findViewById(R.id.directorates);
        spin2.setOnItemSelectedListener(this);
        ArrayAdapter<String> aa2 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list_item);
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin2.setAdapter(aa2);

        //set combo box list of stakeholeder
//        Spinner spin3 = (Spinner) findViewById(id.list_stakeholder);
//        spin3.setOnItemSelectedListener(this);
//        ArrayAdapter<String> aa3 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list_item);
//        aa3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spin3.setAdapter(aa3);

        //set combo box list of area
//        Spinner spin4 = (Spinner) findViewById(R.id.list_area);
//        spin4.setOnItemSelectedListener(this);
//        ArrayAdapter<String> aa4 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list_item);
//        aa4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spin4.setAdapter(aa4);

        //set calendar
        et_date = (EditText)findViewById(R.id.date);

        et_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener dateSetListenerS = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        et_date.setText(dateFormat.format(calendar.getTime()));
                    }
                };
                new DatePickerDialog(FormInteraction.this, dateSetListenerS, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        et_list_stakeholder = (EditText)findViewById(R.id.et_list_stakeholder);
        et_list_stakeholder.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    if(event.getRawX() >= (et_list_stakeholder.getRight() - et_list_stakeholder.getCompoundDrawables()[2].getBounds().width())){
                        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(FormInteraction.this, R.style.FormDialog));
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.popup_list_stakeholder, null);
                        builder.setView(dialogView);
                        Spinner popupSpinner = (Spinner)dialogView.findViewById(R.id.popup_sp_list_stakeholder);
                        final EditText etSpinner = (EditText)dialogView.findViewById(R.id.popup_et_temp);
                        etSpinner.setTypeface(FontAwesome);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(FormInteraction.this, android.R.layout.simple_spinner_item, list_stakeholer);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        popupSpinner.setAdapter(adapter);
                        popupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (++check > 1) {
                                    final String value = list_stakeholer[position];
                                    AlertDialog.Builder b = new AlertDialog.Builder(new ContextThemeWrapper(FormInteraction.this, R.style.FormDialog));
                                    b.setTitle("Degree of Support");
                                    b.setItems(degree_of_support, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            String res = value +" "+icon_degree[which];
                                            if (etSpinner.getText().length() > 0)
                                                etSpinner.append(" " + helper.fromHtml(res));
                                            else etSpinner.append(helper.fromHtml(res));
                                        }
                                    });
                                    AlertDialog a = b.create();
                                    a.show();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String v = etSpinner.getText().toString();
                                et_list_stakeholder.setTypeface(FontAwesome);
                                et_list_stakeholder.setTextColor(Color.BLACK);
                                et_list_stakeholder.setText(helper.fromHtml(v));
                                check = 0;
                            }
                        });

                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                        return true;
                    }
                }
                return false;
            }
        });

        et_list_stakeholder_area = (EditText)findViewById(R.id.et_list_stakeholder_area);
        et_list_stakeholder_area.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    if(event.getRawX() >= (et_list_stakeholder_area.getRight() - et_list_stakeholder_area.getCompoundDrawables()[2].getBounds().width())){
                        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(FormInteraction.this, R.style.FormDialog));
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.popup_list_stakeholder, null);
                        builder.setView(dialogView);
                        Spinner popupSpinner = (Spinner)dialogView.findViewById(R.id.popup_sp_list_stakeholder);
                        final EditText etSpinner = (EditText)dialogView.findViewById(R.id.popup_et_temp);
                        etSpinner.setTypeface(FontAwesome);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(FormInteraction.this, android.R.layout.simple_spinner_item, list_stakeholer);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        popupSpinner.setAdapter(adapter);
                        popupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (++check > 1) {
                                    final String value = list_stakeholer[position];
//                                    AlertDialog.Builder b = new AlertDialog.Builder(new ContextThemeWrapper(FormInteraction.this, R.style.FormDialog));
//                                    b.setTitle("Degree of Support");
//                                    b.setItems(degree_of_support, new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
                                            String res = value;
//                                            if (etSpinner.getText().length() > 0)
                                                etSpinner.append(" " + helper.fromHtml(res));
//                                            else etSpinner.append(helper.fromHtml(res));
//                                        }
//                                    });
//                                    AlertDialog a = b.create();
//                                    a.show();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String v = etSpinner.getText().toString();
                                et_list_stakeholder_area.setTypeface(FontAwesome);
                                et_list_stakeholder_area.setTextColor(Color.BLACK);
                                et_list_stakeholder_area.setText(helper.fromHtml(v));
                                check = 0;
                            }
                        });

                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                        return true;
                    }
                }
                return false;
            }
        });

        /*et_deadline = (EditText)findViewById(id.deadline);

        et_deadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener dateSetListenerS = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        et_deadline.setText(dateFormat.format(calendar.getTime()));
                    }
                };
                new DatePickerDialog(FormInteraction.this, dateSetListenerS, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });*/

        btn_simpan.setOnClickListener( new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_upload.setOnClickListener( new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Toast.makeText(FormInteraction.this,"Uploading File",Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*private void initDefaultHintSpinner() {
        defaults = new ArrayList<>();
        defaults.add(String.format(VALUE_STRING, 1));
        defaults.add(String.format(VALUE_STRING, 2));
        defaults.add(String.format(VALUE_STRING, 3));

        Spinner defaultSpinner = (Spinner) findViewById(R.id.default_spinner);

        defaultHintSpinner = new HintSpinner<>(
                defaultSpinner,
                // Default layout - You don't need to pass in any layout id, just your hint text and
                // your list data
                new HintAdapter<>(this, R.string.default_spinner_hint, defaults),
                new HintSpinner.Callback<String>() {
                    @Override
                    public void onItemSelected(int position, String itemAtPosition) {
                        // Here you handle the on item selected event (this skips the hint selected
                        // event)
                        showSelectedItem(itemAtPosition);
                    }
                });

        Button addMoreButton = (Button) findViewById(R.id.add_new_value_button);
        addMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String randomValue = String.format(VALUE_STRING, Util.generateRandomPositive());
                defaults.add(randomValue);
                defaultHintSpinner.selectHint();
            }
        });
        defaultHintSpinner.init();
    }*/

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
