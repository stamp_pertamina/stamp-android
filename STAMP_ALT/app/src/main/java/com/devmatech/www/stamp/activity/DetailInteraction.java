package com.devmatech.www.stamp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.adapter.InteractionDownloadAdapter;
import com.devmatech.www.stamp.models.InteractionDownload;
import com.devmatech.www.stamp.other.NonScrollListView;

import java.util.ArrayList;

/**
 * Created by Dimas on 6/1/2017.
 */

public class DetailInteraction extends AppCompatActivity {

    NonScrollListView listView;
    InteractionDownloadAdapter adapter;
    ArrayList<InteractionDownload> data;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_interaction);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadData();

        listView = (NonScrollListView) findViewById(R.id.list_row_detail);
        adapter = new InteractionDownloadAdapter(DetailInteraction.this, data);
        listView.setAdapter(adapter);
//        setListViewHeightBasedOnChildren(listView);

        //untuk ngeset judul action bar
        /*Intent i = getIntent();
        Bundle b = i.getExtras();
        IndividualsProfile prof = (IndividualsProfile) b.get("profile");
        name = prof.getName();*/
        getSupportActionBar().setTitle("Johan, Savitri, Eko, Dian");

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadData(){
        data = new ArrayList<>();
        InteractionDownload i = new InteractionDownload();
        i.setName("Interaction Report Title.pdf");
        i.setSize("128 kb");
        data.add(i);
        i = new InteractionDownload();
        i.setName("Interaction Report 2017.pdf");
        i.setSize("200 kb");
        data.add(i);
    }
}
