package com.devmatech.www.stamp.activity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.adapter.PagerAdapter;
import com.devmatech.www.stamp.models.IPModel;
import com.devmatech.www.stamp.models.PrioritizationMatrix;
import com.devmatech.www.stamp.models.SOModel;
import com.devmatech.www.stamp.other.FlowLayout;
import com.devmatech.www.stamp.other.Helper;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class Detail_Strategic extends AppCompatActivity {

    private final int numOfPages = 3; //viewpager has 4 pages
    private final String[] pageTitle = {"Goals", "Description", "Agenda Person"};
    ExpandableRelativeLayout expandableLayout1, expandableLayout2;
    LinearLayout layout_legend;
    ImageView button_download;
    ArrayList<SOModel> itemList = new ArrayList<>();
    Helper helper = new Helper();
    TableLayout tbl_matrix, tbl_degree_support;
    TableRow row;
    Typeface FontAwesome;
    TextView txt_full,txt_fair,txt_more;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_strategic);
        this.setTitle("Mahakam Blok Takeover");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FontAwesome = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf" );
        loadDataSO();
        tbl_matrix = (TableLayout)findViewById(R.id.tbl_matrix);
        tbl_degree_support = (TableLayout)findViewById(R.id.tbl_degree_of_support);
        txt_full = (TextView)findViewById(R.id.legend_full_support);
        txt_fair = (TextView)findViewById(R.id.legend_fair_support);
        txt_more = (TextView)findViewById(R.id.legend_need_more_support);
        txt_full.setTypeface(FontAwesome);
        txt_full.setText(helper.fromHtml(getFontAwesomeIcon("full"))+" Full Support");
        txt_fair.setTypeface(FontAwesome);
        txt_fair.setText(helper.fromHtml(getFontAwesomeIcon("half"))+" Fair Support");
        txt_more.setTypeface(FontAwesome);
        txt_more.setText(helper.fromHtml(getFontAwesomeIcon("empty"))+" Need more support");

        layout_legend = (LinearLayout)findViewById(R.id.layout_legend);
        button_download = (ImageView) findViewById(R.id.item_pdf);
        button_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Detail_Strategic.this,"Downloading pdf..",Toast.LENGTH_SHORT).show();
            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs_strategic);
        tabLayout.setTabTextColors(getResources().getColor(R.color.colorDefault),getResources().getColor(R.color.colorPrimaryDark));

        tabLayout.addTab(tabLayout.newTab().setText("Goals"));
        tabLayout.addTab(tabLayout.newTab().setText("Description"));
        tabLayout.addTab(tabLayout.newTab().setText("Agenda Person In Charge"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        for (int i = 0; i < numOfPages; i++) {
//            tabLayout.addTab(tabLayout.newTab().setText(pageTitle[i]));
//        }
//
//        //set gravity for tab bar
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//
//        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
//        final PagerAdapter adapter = new PagerAdapter
//                (getSupportFragmentManager(), numOfPages);
//
//        viewPager.setAdapter(adapter);
//        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
//        tabLayout.setOnTabSelectedListener(onTabSelectedListener(viewPager));
//    }

//    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager pager) {
//        return new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                pager.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        };

        int height = helper.getSizeScreen(this, 1);
        int width = helper.getSizeScreen(this, 0);

        SOModel so = itemList.get(0);
        String[][] matrix = new String[3][3];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrix[i][j] = "";
            }
        }
        List<PrioritizationMatrix> list = so.getPrioritizationMatrix();
        for (int i = 0; i < list.size(); i++) {
            PrioritizationMatrix data = list.get(i);
            int r = Integer.parseInt(data.getRow()) - 1;
            int c = Integer.parseInt(data.getCol()) - 1;
            matrix[r][c] += data.getId()+";";
        }
        int row_width = (int) Math.round((width *31.25)/100);
        int row_height = (int) Math.round((height * 18.75)/100);

        float density = helper.getDensityScreen(this, 2);
        for (int i = ((matrix.length) - 1); i >= 0; i--) {
            row = new TableRow(this);
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.MATCH_PARENT,5f));
            row.setWeightSum(1);
            for (int j = 0; j < matrix.length; j++) {
                RelativeLayout v = new RelativeLayout(this);
                v.setLayoutParams(new TableRow.LayoutParams(row_width, row_height));//150,145
                v.setBackgroundResource(R.drawable.value_shape);
                v.setGravity(RelativeLayout.CENTER_VERTICAL);
                v.setPadding(5, 5, 5, 5);
                FlowLayout f = new FlowLayout(this);
                RelativeLayout.LayoutParams layout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layout.setMargins(5,0,0,0);
                f.setLayoutParams(layout);
                v.addView(f);
                if(matrix[i][j].length() != 0){
                    String[] ar = matrix[i][j].substring(0,(matrix[i][j].length()-1)).split(";");
                    for (int k = 0; k < ar.length; k++) {
                        TextView t = new TextView(this);
                        t.setText(String.valueOf(ar[k]));
                        if(density < 390) {
                            if (ar[k].length() > 1) {
                                t.setPadding(12, 10, 12, 10);
                            } else {
                                t.setPadding(20, 10, 20, 10);
                            }
                        }else if(density > 400 ){
                            if (ar[k].length() > 1) {
                                t.setPadding(20, 15, 20, 15);
                            } else {
                                t.setPadding(30, 15, 30, 15);
                            }
                        }
                        t.setTextColor(Color.WHITE);
                        t.setBackgroundResource(R.drawable.blue_circle);
                        f.addView(t);
                    }
                }
                row.addView(v);
            }
            tbl_matrix.addView(row);
        }

        if(density < 390) {
            layout_legend.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0,10,0,0);
            txt_fair.setLayoutParams(params);
            txt_more.setLayoutParams(params);
        }else if(density > 400 ){
            layout_legend.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(20,0,0,0);
            txt_fair.setLayoutParams(params);
            txt_more.setLayoutParams(params);
        }
//        Looping for degree of support
        for (int i = 0; i < list.size(); i++) {
            row = new TableRow(this);
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,TableRow.LayoutParams.WRAP_CONTENT,5f));
            row.setWeightSum(1);
            TextView t = new TextView(this);
            t.setText(String.valueOf(i+1));
            t.setPadding(5,8,18,5);
            t.setBackgroundResource(R.drawable.value_shape);
            row.addView(t);
            TextView n = new TextView(this);
            n.setText(list.get(i).getName());
            n.setPadding(5,8,18,5);
            n.setBackgroundResource(R.drawable.value_shape);
            row.addView(n);
            TextView q1 = new TextView(this);
            q1.setGravity(Gravity.CENTER);
            q1.setPadding(5,15,18,10);
            q1.setBackgroundResource(R.drawable.value_shape);
            q1.setTypeface(FontAwesome);
            q1.setText(helper.fromHtml(getFontAwesomeIcon(list.get(i).getQ1())));
            row.addView(q1);
            TextView q2 = new TextView(this);
            q2.setGravity(Gravity.CENTER);
            q2.setPadding(5,15,18,10);
            q2.setBackgroundResource(R.drawable.value_shape);
            q2.setTypeface(FontAwesome);
            q2.setText(helper.fromHtml(getFontAwesomeIcon(list.get(i).getQ2())));
            row.addView(q2);
            TextView q3 = new TextView(this);
            q3.setGravity(Gravity.CENTER);
            q3.setPadding(5,15,18,10);
            q3.setBackgroundResource(R.drawable.value_shape);
            q3.setTypeface(FontAwesome);
            q3.setText(helper.fromHtml(getFontAwesomeIcon(list.get(i).getQ3())));
            row.addView(q3);
            TextView q4 = new TextView(this);
            q4.setGravity(Gravity.CENTER);
            q4.setPadding(5,15,18,10);
            q4.setBackgroundResource(R.drawable.value_shape);
            q4.setTypeface(FontAwesome);
            q4.setText(helper.fromHtml(getFontAwesomeIcon(list.get(i).getQ4())));
            row.addView(q4);
            tbl_degree_support.addView(row);
        }
    }

    public String getFontAwesomeIcon(String icon){
        if (icon.equals("full")){
            return "&#xf111;";
        }else if(icon.equals("half")){
            return "&#xf042;";
        }else if(icon.equals("empty")){
            return "&#xf10c;";
        }
        return "";
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void expandBtnPrioritizedMatrix(View view) {
        expandableLayout1 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout1);
        expandableLayout1.toggle(); // toggle expand and collapse
    }

    public void expandEngageTimeline(View view) {
        expandableLayout2 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout2);
        expandableLayout2.toggle(); // toggle expand and collapse
    }

    public void loadDataSO(){
        Gson gson = new Gson();
        Helper helper = new Helper();
        try {
            JSONArray array = new JSONArray(helper.LoadJsonDataFromAssets(Detail_Strategic.this, "MBT-SO.json"));
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                SOModel soModel = gson.fromJson(obj.toString(), SOModel.class);
                itemList.add(soModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
