package com.devmatech.www.stamp;

import android.app.Application;

import com.devmatech.www.stamp.other.TypeFaceUtil;

/**
 * Created by ASUS-PC on 15/06/2017.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        TypeFaceUtil.setDefaultFont(getApplicationContext(), "MONOSPACE", "Roboto-Regular.ttf");
    }
}
