package com.devmatech.www.stamp.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.activity.FormInteraction;
import com.devmatech.www.stamp.adapter.InteractionAdapter;

/**
 * Created by Dimas on 30/05/2017.
 */

public class Interaction extends Fragment{

    ListView list;
    String [] itemname = {
            "Johan, Savitri, Eko Dian",
            "Johan, Savitri, Eko Dian",
            "Johan, Savitri, Eko Dian",
            "Johan, Savitri, Eko Dian",
            "Johan, Savitri, Eko Dian",
            "Johan, Savitri, Eko Dian",
    };

    Integer [] itemdate = {
            1,
            2,
            3,
            4,
            5,
    };

    String [] itemcontent = {
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.",
    };

    FloatingActionButton fab;
    public Interaction(){}

    RelativeLayout view;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = (RelativeLayout) inflater.inflate(R.layout.interaction_main, container, false);
        getActivity().setTitle("List of Interaction Report");

        InteractionAdapter adapter = new InteractionAdapter(getActivity(), itemname);
        list = (ListView) view.findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                String Selecteditem= itemname[+position];
                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.DetailInteraction.class);
                startActivity(myIntent);
//                ((DetailInteraction) getActivity()).setActionBarTitle(Selecteditem);
//                Toast.makeText(getActivity(), Selecteditem, Toast.LENGTH_SHORT).show();

            }
        });

        fab = (FloatingActionButton) view.findViewById(R.id.fab_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FormInteraction.class);
                getActivity().startActivity(intent);
            }
        });
        return view;
    }

}
