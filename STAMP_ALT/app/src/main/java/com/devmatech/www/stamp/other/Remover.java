package com.devmatech.www.stamp.other;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

/**
 * Created by Whelly on 08/06/2017.
 */

public class Remover extends PercentFormatter {

    private DecimalFormat mDecimalFormat;

    public Remover (DecimalFormat format) {
        this.mDecimalFormat = format;
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {

        return  mDecimalFormat.format(value) ;
    }
}
