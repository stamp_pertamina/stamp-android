package com.devmatech.www.stamp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.models.InteractionDownload;

import java.util.ArrayList;

/**
 * Created by ASUS-PC on 21/06/2017.
 */

public class InteractionDownloadAdapter extends ArrayAdapter<InteractionDownload> {

    public InteractionDownloadAdapter(@NonNull Context context, ArrayList<InteractionDownload> data) {
        super(context, 0,data);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        InteractionDownload row = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_detail_interaction, parent, false);
        }
        TextView name = (TextView)convertView.findViewById(R.id.row_detail_name);
        TextView size = (TextView)convertView.findViewById(R.id.row_detail_size);

        name.setText(row.getName());
        size.setText(row.getSize());

        return convertView;
    }
}
