package com.devmatech.www.stamp.models;

import java.io.Serializable;

/**
 * Created by Dimas on 6/14/2017.
 */

public class LegendList implements Serializable {
    String id;
    String name_legend;
    Integer icon_list;

    public void setName_legend(String name_legend) {
        this.name_legend = name_legend;
    }

    public void setIcon_list(Integer icon_list) {
        this.icon_list = icon_list;
    }


    public String getName_legend() {
        return name_legend;
    }

    public Integer getIcon_list() {
        return icon_list;
    }
}
