package com.devmatech.www.stamp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devmatech.www.stamp.R;

/**
 * Created by Whelly on 05/06/2017.
 */

public class TabStrategicFragment_2 extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_strategic_fragment_2, container, false);
    }
}
