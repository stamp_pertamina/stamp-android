
package com.devmatech.www.stamp.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IPModel implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("stamp_node")
    @Expose
    private String stampNode;
    @SerializedName("stakeholder_type")
    @Expose
    private String stakeholderType;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("organization_name")
    @Expose
    private String organizationName;
    @SerializedName("individual_name")
    @Expose
    private String individualName;
    @SerializedName("title_name")
    @Expose
    private String titleName;
    @SerializedName("birth_date")
    @Expose
    private String birthDate;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("spouse_name")
    @Expose
    private String spouseName;
    @SerializedName("children_name_1")
    @Expose
    private String childrenName1;
    @SerializedName("children_name_2")
    @Expose
    private String childrenName2;
    @SerializedName("children_name_3")
    @Expose
    private String childrenName3;
    @SerializedName("children_name_4")
    @Expose
    private String childrenName4;
    @SerializedName("children_name_5")
    @Expose
    private String childrenName5;
    @SerializedName("children_name_6")
    @Expose
    private String childrenName6;
    @SerializedName("children_name_7")
    @Expose
    private String childrenName7;
    @SerializedName("children_name_8")
    @Expose
    private String childrenName8;
    @SerializedName("children_name_9")
    @Expose
    private String childrenName9;
    @SerializedName("children_name_10")
    @Expose
    private String childrenName10;
    @SerializedName("personal_interest")
    @Expose
    private String personalInterest;
    @SerializedName("education_detail_1")
    @Expose
    private String educationDetail1;
    @SerializedName("education_detail_2")
    @Expose
    private String educationDetail2;
    @SerializedName("education_detail_3")
    @Expose
    private String educationDetail3;
    @SerializedName("achievement")
    @Expose
    private String achievement;
    @SerializedName("working_experience_1")
    @Expose
    private String workingExperience1;
    @SerializedName("working_experience_2")
    @Expose
    private String workingExperience2;
    @SerializedName("working_experience_3")
    @Expose
    private String workingExperience3;
    @SerializedName("working_experience_4")
    @Expose
    private String workingExperience4;
    @SerializedName("working_experience_5")
    @Expose
    private String workingExperience5;
    @SerializedName("working_experience_6")
    @Expose
    private String workingExperience6;
    @SerializedName("working_experience_7")
    @Expose
    private String workingExperience7;
    @SerializedName("working_experience_8")
    @Expose
    private String workingExperience8;
    @SerializedName("working_experience_9")
    @Expose
    private String workingExperience9;
    @SerializedName("working_experience_10")
    @Expose
    private String workingExperience10;
    @SerializedName("political_affiliation_1")
    @Expose
    private String politicalAffiliation1;
    @SerializedName("political_affiliation_2")
    @Expose
    private String politicalAffiliation2;
    @SerializedName("political_affiliation_3")
    @Expose
    private String politicalAffiliation3;
    @SerializedName("political_affiliation_4")
    @Expose
    private String politicalAffiliation4;
    @SerializedName("other_organizational_affiliation_1")
    @Expose
    private String otherOrganizationalAffiliation1;
    @SerializedName("other_organizational_affiliation_2")
    @Expose
    private String otherOrganizationalAffiliation2;
    @SerializedName("other_organizational_affiliation_3")
    @Expose
    private String otherOrganizationalAffiliation3;
    @SerializedName("other_organizational_affiliation_4")
    @Expose
    private String otherOrganizationalAffiliation4;
    @SerializedName("personal_preferences")
    @Expose
    private String personalPreferences;
    @SerializedName("stakeholder_responsibility")
    @Expose
    private String stakeholderResponsibility;
    @SerializedName("internal_pertamina_contact")
    @Expose
    private List<InternalPertaminaContact> internalPertaminaContact = null;
    @SerializedName("stakeholder_network")
    @Expose
    private List<StakeholderNetwork> stakeholderNetwork = null;
    @SerializedName("latest_stakeholder_perception")
    @Expose
    private List<LatestStakeholderPerception> latestStakeholderPerception = null;
    @SerializedName("engagement_record")
    @Expose
    private List<EngagementRecord> engagementRecord = null;
    @SerializedName("list_area_of_concern")
    @Expose
    private List<ListAreaOfConcern> listAreaOfConcern = null;
    @SerializedName("closest_relative_contact")
    @Expose
    private String closestRelativeContact;
    @SerializedName("strategic_objective")
    @Expose
    private String strategicObjective;
    @SerializedName("perception")
    @Expose
    private String perception;
    @SerializedName("topic")
    @Expose
    private String topic;
    @SerializedName("engagement_type")
    @Expose
    private String engagementType;
    @SerializedName("list_areas_of_concerns")
    @Expose
    private String listAreasOfConcerns;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("list_source")
    @Expose
    private List<ListSource> listSource = null;
    @SerializedName("last_update")
    @Expose
    private String lastUpdate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStampNode() { return stampNode; }

    public void setStampNode(String stampNode) { this.stampNode = stampNode; }

    public String getStakeholderType() {
        return stakeholderType;
    }

    public void setStakeholderType(String stakeholderType) {
        this.stakeholderType = stakeholderType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getIndividualName() {
        return individualName;
    }

    public void setIndividualName(String individualName) {
        this.individualName = individualName;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getChildrenName1() {
        return childrenName1;
    }

    public void setChildrenName1(String childrenName1) {
        this.childrenName1 = childrenName1;
    }

    public String getChildrenName2() {
        return childrenName2;
    }

    public void setChildrenName2(String childrenName2) {
        this.childrenName2 = childrenName2;
    }

    public String getChildrenName3() {
        return childrenName3;
    }

    public void setChildrenName3(String childrenName3) {
        this.childrenName3 = childrenName3;
    }

    public String getChildrenName4() {
        return childrenName4;
    }

    public void setChildrenName4(String childrenName4) {
        this.childrenName4 = childrenName4;
    }

    public String getChildrenName5() {
        return childrenName5;
    }

    public void setChildrenName5(String childrenName5) {
        this.childrenName5 = childrenName5;
    }

    public String getChildrenName6() {
        return childrenName6;
    }

    public void setChildrenName6(String childrenName6) {
        this.childrenName6 = childrenName6;
    }

    public String getChildrenName7() {
        return childrenName7;
    }

    public void setChildrenName7(String childrenName7) {
        this.childrenName7 = childrenName7;
    }

    public String getChildrenName8() {
        return childrenName8;
    }

    public void setChildrenName8(String childrenName8) {
        this.childrenName8 = childrenName8;
    }

    public String getChildrenName9() {
        return childrenName9;
    }

    public void setChildrenName9(String childrenName9) {
        this.childrenName9 = childrenName9;
    }

    public String getChildrenName10() {
        return childrenName10;
    }

    public void setChildrenName10(String childrenName10) {
        this.childrenName10 = childrenName10;
    }

    public String getPersonalInterest() {
        return personalInterest;
    }

    public void setPersonalInterest(String personalInterest) {
        this.personalInterest = personalInterest;
    }

    public String getEducationDetail1() {
        return educationDetail1;
    }

    public void setEducationDetail1(String educationDetail1) {
        this.educationDetail1 = educationDetail1;
    }

    public String getEducationDetail2() {
        return educationDetail2;
    }

    public void setEducationDetail2(String educationDetail2) {
        this.educationDetail2 = educationDetail2;
    }

    public String getEducationDetail3() {
        return educationDetail3;
    }

    public void setEducationDetail3(String educationDetail3) {
        this.educationDetail3 = educationDetail3;
    }

    public String getAchievement() {
        return achievement;
    }

    public void setAchievement(String achievement) {
        this.achievement = achievement;
    }

    public String getWorkingExperience1() {
        return workingExperience1;
    }

    public void setWorkingExperience1(String workingExperience1) {
        this.workingExperience1 = workingExperience1;
    }

    public String getWorkingExperience2() {
        return workingExperience2;
    }

    public void setWorkingExperience2(String workingExperience2) {
        this.workingExperience2 = workingExperience2;
    }

    public String getWorkingExperience3() {
        return workingExperience3;
    }

    public void setWorkingExperience3(String workingExperience3) {
        this.workingExperience3 = workingExperience3;
    }

    public String getWorkingExperience4() {
        return workingExperience4;
    }

    public void setWorkingExperience4(String workingExperience4) {
        this.workingExperience4 = workingExperience4;
    }

    public String getWorkingExperience5() {
        return workingExperience5;
    }

    public void setWorkingExperience5(String workingExperience5) {
        this.workingExperience5 = workingExperience5;
    }

    public String getWorkingExperience6() {
        return workingExperience6;
    }

    public void setWorkingExperience6(String workingExperience6) {
        this.workingExperience6 = workingExperience6;
    }

    public String getWorkingExperience7() {
        return workingExperience7;
    }

    public void setWorkingExperience7(String workingExperience7) {
        this.workingExperience7 = workingExperience7;
    }

    public String getWorkingExperience8() {
        return workingExperience8;
    }

    public void setWorkingExperience8(String workingExperience8) {
        this.workingExperience8 = workingExperience8;
    }

    public String getWorkingExperience9() {
        return workingExperience9;
    }

    public void setWorkingExperience9(String workingExperience9) {
        this.workingExperience9 = workingExperience9;
    }

    public String getWorkingExperience10() {
        return workingExperience10;
    }

    public void setWorkingExperience10(String workingExperience10) {
        this.workingExperience10 = workingExperience10;
    }

    public String getPoliticalAffiliation1() {
        return politicalAffiliation1;
    }

    public void setPoliticalAffiliation1(String politicalAffiliation1) {
        this.politicalAffiliation1 = politicalAffiliation1;
    }

    public String getPoliticalAffiliation2() {
        return politicalAffiliation2;
    }

    public void setPoliticalAffiliation2(String politicalAffiliation2) {
        this.politicalAffiliation2 = politicalAffiliation2;
    }

    public String getPoliticalAffiliation3() {
        return politicalAffiliation3;
    }

    public void setPoliticalAffiliation3(String politicalAffiliation3) {
        this.politicalAffiliation3 = politicalAffiliation3;
    }

    public String getPoliticalAffiliation4() {
        return politicalAffiliation4;
    }

    public void setPoliticalAffiliation4(String politicalAffiliation4) {
        this.politicalAffiliation4 = politicalAffiliation4;
    }

    public String getOtherOrganizationalAffiliation1() {
        return otherOrganizationalAffiliation1;
    }

    public void setOtherOrganizationalAffiliation1(String otherOrganizationalAffiliation1) {
        this.otherOrganizationalAffiliation1 = otherOrganizationalAffiliation1;
    }

    public String getOtherOrganizationalAffiliation2() {
        return otherOrganizationalAffiliation2;
    }

    public void setOtherOrganizationalAffiliation2(String otherOrganizationalAffiliation2) {
        this.otherOrganizationalAffiliation2 = otherOrganizationalAffiliation2;
    }

    public String getOtherOrganizationalAffiliation3() {
        return otherOrganizationalAffiliation3;
    }

    public void setOtherOrganizationalAffiliation3(String otherOrganizationalAffiliation3) {
        this.otherOrganizationalAffiliation3 = otherOrganizationalAffiliation3;
    }

    public String getOtherOrganizationalAffiliation4() {
        return otherOrganizationalAffiliation4;
    }

    public void setOtherOrganizationalAffiliation4(String otherOrganizationalAffiliation4) {
        this.otherOrganizationalAffiliation4 = otherOrganizationalAffiliation4;
    }

    public String getPersonalPreferences() {
        return personalPreferences;
    }

    public void setPersonalPreferences(String personalPreferences) {
        this.personalPreferences = personalPreferences;
    }

    public String getStakeholderResponsibility() {
        return stakeholderResponsibility;
    }

    public void setStakeholderResponsibility(String stakeholderResponsibility) {
        this.stakeholderResponsibility = stakeholderResponsibility;
    }

    public List<InternalPertaminaContact> getInternalPertaminaContact() {
        return internalPertaminaContact;
    }

    public void setInternalPertaminaContact(List<InternalPertaminaContact> internalPertaminaContact) {
        this.internalPertaminaContact = internalPertaminaContact;
    }

    public List<StakeholderNetwork> getStakeholderNetwork() {
        return stakeholderNetwork;
    }

    public void setStakeholderNetwork(List<StakeholderNetwork> stakeholderNetwork) {
        this.stakeholderNetwork = stakeholderNetwork;
    }

    public List<LatestStakeholderPerception> getLatestStakeholderPerception() {
        return latestStakeholderPerception;
    }

    public void setLatestStakeholderPerception(List<LatestStakeholderPerception> latestStakeholderPerception) {
        this.latestStakeholderPerception = latestStakeholderPerception;
    }

    public List<EngagementRecord> getEngagementRecord() {
        return engagementRecord;
    }

    public void setEngagementRecord(List<EngagementRecord> engagementRecord) {
        this.engagementRecord = engagementRecord;
    }

    public List<ListAreaOfConcern> getListAreaOfConcern() {
        return listAreaOfConcern;
    }

    public void setListAreaOfConcern(List<ListAreaOfConcern> listAreaOfConcern) {
        this.listAreaOfConcern = listAreaOfConcern;
    }

    public String getClosestRelativeContact() {
        return closestRelativeContact;
    }

    public void setClosestRelativeContact(String closestRelativeContact) {
        this.closestRelativeContact = closestRelativeContact;
    }

    public String getStrategicObjective() {
        return strategicObjective;
    }

    public void setStrategicObjective(String strategicObjective) {
        this.strategicObjective = strategicObjective;
    }

    public String getPerception() {
        return perception;
    }

    public void setPerception(String perception) {
        this.perception = perception;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getEngagementType() {
        return engagementType;
    }

    public void setEngagementType(String engagementType) {
        this.engagementType = engagementType;
    }

    public String getListAreasOfConcerns() {
        return listAreasOfConcerns;
    }

    public void setListAreasOfConcerns(String listAreasOfConcerns) {
        this.listAreasOfConcerns = listAreasOfConcerns;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<ListSource> getListSource() {
        return listSource;
    }

    public void setListSource(List<ListSource> listSource) {
        this.listSource = listSource;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
