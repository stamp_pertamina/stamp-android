
package com.devmatech.www.stamp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PrioritizedEngagementTimeline implements Serializable {

    @SerializedName("engagement_date")
    @Expose
    private String engagementDate;
    @SerializedName("engagement_topic")
    @Expose
    private String engagementTopic;
    @SerializedName("engagement_type")
    @Expose
    private String engagementType;

    public String getEngagementDate() {
        return engagementDate;
    }

    public void setEngagementDate(String engagementDate) {
        this.engagementDate = engagementDate;
    }

    public String getEngagementTopic() {
        return engagementTopic;
    }

    public void setEngagementTopic(String engagementTopic) {
        this.engagementTopic = engagementTopic;
    }

    public String getEngagementType() {
        return engagementType;
    }

    public void setEngagementType(String engagementType) {
        this.engagementType = engagementType;
    }

}
