package com.devmatech.www.stamp.adapter;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.models.IPModel;
import com.devmatech.www.stamp.models.IndividualsProfile;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;

/**
 * Created by VILLA-PC on 6/1/2017.
 */

public class IndividualListAdapter extends BaseAdapter {

    private ArrayList<IPModel> listProfile = new ArrayList<>();
    private Context context;
    private LayoutInflater mInflater;
    int hide = 0;

    public IndividualListAdapter(ArrayList<IPModel> listProfile, Context context) {
        this.listProfile = listProfile;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        hide = listProfile.size() - 5;
    }

    public void showAll(){
        hide = 0;
        notifyDataSetChanged();
        notifyDataSetInvalidated();
    }

    @Override
    public int getCount() {
        return listProfile.size() - hide;
    }

    @Override
    public Object getItem(int position) { return listProfile.get(position); }

    @Override
    public long getItemId(int position) { return position; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemHolder holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.individual_profile, parent,false);
            holder = new ItemHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ItemHolder)convertView.getTag();
        }
        final IPModel item = listProfile.get(position);
        holder.profile_nama.setText(item.getIndividualName());
        holder.profile_jabatan.setText(item.getTitleName());
        Uri uri;
        if(item.getId().length() > 0){
//            File file = new File(URI.create("file:///android_asset/IP/" + item.getId()+".jpg"));
//            if (file.exists()){
                uri = Uri.parse("file:///android_asset/IP/" + item.getId()+".jpg");
//            }else{
//                uri = Uri.parse("file:///android_asset/placeholder-person.jpg");
//            }
        }else {
            uri = Uri.parse("file:///android_asset/placeholder-person.jpg");
        }
        //link url untuk akses gambar
        //nanti kalau sudah siap langsung tambahkan nama file di belakang url
        String imageurl = "";
        Picasso.with(context).load(uri).resize(100,100)
                .placeholder(R.drawable.progress_animation).centerCrop().into(holder.profile_avatar);

        return convertView;
    }

    class ItemHolder{
        private TextView profile_nama, profile_jabatan;
        ImageView profile_avatar;
        public ItemHolder(View view) {
            profile_nama = (TextView)view.findViewById(R.id.item_profile_nama);
            profile_jabatan = (TextView)view.findViewById(R.id.item_profile_jabatan);;
            profile_avatar = (ImageView)view.findViewById(R.id.item_profile_image);
        }
    }
}
