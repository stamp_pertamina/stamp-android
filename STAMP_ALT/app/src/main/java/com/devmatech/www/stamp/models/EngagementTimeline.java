package com.devmatech.www.stamp.models;

import java.io.Serializable;

/**
 * Created by ASUS-PC on 16/06/2017.
 */

public class EngagementTimeline implements Serializable {
        private String id;

        private String time;

        private String organization;

        private String objective;

        private String pic;

        private String type;

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getTime ()
        {
            return time;
        }

        public void setTime (String time)
        {
            this.time = time;
        }

        public String getOrganization ()
        {
            return organization;
        }

        public void setOrganization (String organization)
        {
            this.organization = organization;
        }

        public String getObjective ()
        {
            return objective;
        }

        public void setObjective (String objective)
        {
            this.objective = objective;
        }

        public String getPic ()
        {
            return pic;
        }

        public void setPic (String pic)
        {
            this.pic = pic;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }
}
