package com.devmatech.www.stamp.models;

/**
 * Created by ASUS-PC on 16/06/2017.
 */

public class InternalSmes
{
    private String id;

    private String title;

    private String name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", title = "+title+", name = "+name+"]";
    }
}
