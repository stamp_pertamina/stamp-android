package com.devmatech.www.stamp.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.Toast;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.adapter.LegendListAdapter;
//import com.devmatech.www.stamp.adapter.NumberedAdapter;
import com.devmatech.www.stamp.models.LegendList;
import com.devmatech.www.stamp.other.Remover;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.devmatech.www.stamp.R.id.tableRow1;
import static com.devmatech.www.stamp.R.id.tableRow2;
import static com.devmatech.www.stamp.R.id.tableRow3;
import static com.devmatech.www.stamp.R.id.tableRow4;
import static com.devmatech.www.stamp.R.id.tableRow5;

/**
 * Created by Whelly on 13/06/2017.
 */

public class EngagementMedia extends Fragment {
    private Spinner spinner1;
    private LinearLayout linlay;
    PieChart pieChart,pieChart2,pieChart3,pieChart4 ;
    ArrayList<Entry> entries,entries_char2,entries_char3,entries_char4 ;
    ArrayList<String> PieEntryLabels,PieEntryLabels_char2,PieEntryLabels_char3,PieEntryLabels_char4 ;
    PieDataSet pieDataSet,pieDataSetChar2,pieDataSetChar3,pieDataSetChar4 ;
    PieData pieData,pieDataChar2,pieDataChar3,pieDataChar4 ;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Fragment fragment = null;

    ExpandableRelativeLayout expandableLayout;
    Button b1,b2,b3,b4;
    ImageView img1,img2,img3,img4,img5,img6;

    private GridView gridView,gridView2,gridView3;

    ArrayList<LegendList> LegendsList = new ArrayList<>();
    ArrayList<LegendList> LegendsList2 = new ArrayList<>();
    ArrayList<LegendList> LegendsList3 = new ArrayList<>();

    LegendListAdapter adapterLegend,adapterLegend2,adapterLegend3;

    private Context mContext;
    private PopupWindow mPopupWindow;
    private LinearLayout mRelativeLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.engagement_media, container, false);

        gridView = (GridView) view.findViewById(R.id.grid_view);
        gridView2 = (GridView) view.findViewById(R.id.grid_view2);
        gridView3 = (GridView) view.findViewById(R.id.grid_view3);
        listLegendChart1();
        listLegendChart2();
        listLegendChart3();
        adapterLegend = new LegendListAdapter(LegendsList, getActivity().getBaseContext());
        adapterLegend2 = new LegendListAdapter(LegendsList2, getActivity().getBaseContext());
        adapterLegend3 = new LegendListAdapter(LegendsList3, getActivity().getBaseContext());
        gridView.setAdapter(adapterLegend);
        gridView2.setAdapter(adapterLegend2);
        gridView3.setAdapter(adapterLegend3);

        spinner1 = (Spinner) view.findViewById(R.id.spinner_inter);
        spinner1.setOnItemSelectedListener(new EngagementMedia.CustomOnItemSelectedListener());
        getActivity().setTitle("Engagement Summary");

        mContext = getActivity();
        mRelativeLayout = (LinearLayout) view.findViewById(R.id.cover);

        final BottomNavigationView bottomNavigationView = (BottomNavigationView) view.findViewById( R.id.bottomMenuEngagement );

        //piechart1
        pieChart = (PieChart) view.findViewById(R.id.chartMedia1);

        pieChart.setHoleRadius(40f);
        pieChart.setDescription("");
        pieChart.setCenterText("View\nDetails");
        pieChart.setDrawCenterText(true);
        pieChart.setClickable(true);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
//                Log.i("I clicked ons", String.valueOf(e.getXIndex()) );
                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.DetailEngagementMedia.class);
                myIntent.putExtra("id", "c1");
                startActivity(myIntent);
            }

            @Override
            public void onNothingSelected() {

            }
        });
        entries = new ArrayList<>();
        PieEntryLabels = new ArrayList<String>();
        AddValuesToPIEENTRY();
        AddValuesToPieEntryLabels();

        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new Remover(new DecimalFormat("###,###,###")));
        pieDataSet.setColors(new int[] {Color.rgb(0,108,183), Color.rgb(171,194,44), Color.rgb(237,27,46), Color.rgb(101,186,245), Color.rgb(146,152,111), Color.rgb(139,195,74), Color.rgb(34,149,136), Color.rgb(6,32,21), Color.rgb(180,180,180)});

        pieChart.setDrawSliceText(false);

        pieData.setValueTextSize(12f);
        pieData.setValueTextColor(Color.rgb(255,255,255));
        Legend legend = pieChart.getLegend();
        legend.setEnabled(false);
        legend.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        legend.setXEntrySpace(7f);
        legend.setYEntrySpace(0f);
        legend.setYOffset(0f);
        legend.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
        legend.setWordWrapEnabled(true);

//        listLegendChart1();
        pieChart.setData(pieData);
//        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setAdapter(new NumberedAdapter(LegendsList,getActivity().getBaseContext()));

        //piechart2
        //Pie Chart2
        pieChart2 = (PieChart) view.findViewById(R.id.chartMedia2);
        pieChart2.setHoleRadius(40f);
        pieChart2.setDescription("");
        pieChart2.setCenterText("View\nDetails");
        pieChart2.setDrawCenterText(true);
        pieChart2.setClickable(true);
        pieChart2.setDrawHoleEnabled(true);
        pieChart2.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
//                Log.i("I clicked ons", String.valueOf(e.getXIndex()) );
                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.DetailEngagementMedia.class);
                myIntent.putExtra("id", "c2");
                startActivity(myIntent);
            }

            @Override
            public void onNothingSelected() {

            }
        });
        entries_char2 = new ArrayList<>();
        PieEntryLabels_char2 = new ArrayList<>();
        AddValuesToPIEENTRYCHAR2();
        AddValuesToPieEntryLabelsChar2();
        pieDataSetChar2 = new PieDataSet(entries_char2, "");

        pieDataChar2 = new PieData(PieEntryLabels_char2, pieDataSetChar2);
        pieDataChar2.setValueFormatter(new Remover(new DecimalFormat("###,###,###")));
        pieDataSetChar2.setColors(new int[] {Color.rgb(0,108,183), Color.rgb(34,149,136), Color.rgb(171,194,44), Color.rgb(6,32,21), Color.rgb(237,27,46), Color.rgb(175,175,175), Color.rgb(101,186,245), Color.rgb(225,126,126), Color.rgb(146,152,111)});

        pieChart2.setDrawSliceText(false);

        pieDataChar2.setValueTextSize(12f);
        pieDataChar2.setValueTextColor(Color.rgb(255,255,255));
        Legend legendchar2 = pieChart2.getLegend();
        legendchar2.setEnabled(false);
        legendchar2.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        legendchar2.setXEntrySpace(7f);
        legendchar2.setYEntrySpace(0f);
        legendchar2.setYOffset(0f);
        legendchar2.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
        legendchar2.setWordWrapEnabled(true);


        pieChart2.setData(pieDataChar2);

        //pieChart2.animateY(3000);


        //Pie Chart3
        pieChart3 = (PieChart) view.findViewById(R.id.chartMedia3);
        pieChart3.setHoleRadius(40f);
        pieChart3.setDescription("");
        pieChart3.setCenterText("View\nDetails");
        pieChart3.setDrawCenterText(true);
        pieChart3.setClickable(true);
        pieChart3.setDrawHoleEnabled(true);
        pieChart3.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
//                Log.i("I clicked ons", String.valueOf(e.getXIndex()) );
                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.DetailEngagementMedia.class);
                myIntent.putExtra("id", "c3");
                startActivity(myIntent);
            }

            @Override
            public void onNothingSelected() {

            }
        });
        entries_char3 = new ArrayList<>();
        PieEntryLabels_char3 = new ArrayList<>();
        AddValuesToPIEENTRYCHAR3();
        AddValuesToPieEntryLabelsChar3();
        pieDataSetChar3 = new PieDataSet(entries_char3, "");

        pieDataChar3 = new PieData(PieEntryLabels_char3, pieDataSetChar3);
        pieDataChar3.setValueFormatter(new Remover(new DecimalFormat("###,###,###")));
        pieDataSetChar3.setColors(new int[] {Color.rgb(0,108,183), Color.rgb(171,149,44), Color.rgb(237,27,46), Color.rgb(101,186,245), Color.rgb(146,152,111), Color.rgb(74,145,200), Color.rgb(34,149,136), Color.rgb(6,32,21), Color.rgb(175,175,175)});

        pieChart3.setDrawSliceText(false);

        pieDataChar3.setValueTextSize(12f);
        pieDataChar3.setValueTextColor(Color.rgb(255,255,255));
        Legend legendchar3 = pieChart3.getLegend();
        legendchar3.setEnabled(false);
        legendchar3.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        legendchar3.setXEntrySpace(7f);
        legendchar3.setYEntrySpace(0f);
        legendchar3.setYOffset(0f);
        legendchar3.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
        legendchar3.setWordWrapEnabled(true);


        pieChart3.setData(pieDataChar3);

        //pieChart3.animateY(3000);

        bottomNavigationView.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener(){

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()){
                    case R.id.menu_all:

//                        PopupMenu popup = new PopupMenu(getActivity(),  bottomNavigationView);
//                        // Inflating menu using xml file
//                        popup.getMenuInflater().inflate(R.menu.all_scf_menu, popup.getMenu());
//
//                        // registering OnMenuItemClickListener
//                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                fragmentManager = getFragmentManager();
//                                switch (item.getTitle().toString()){
//                                    case "Institutional Relation" :
//                                        fragment = new Engagement();
//                                        callFragment(fragment);
//                                        break;
//                                    case "Investor Relation" :
//                                        fragment = new EngagementInvestor();
//                                        callFragment(fragment);
//                                        break;
//                                    case "International" :
//                                        fragment = new EngagementInternational();
//                                        callFragment(fragment);
//                                        break;
//                                    case "Media" :
//                                        fragment = new EngagementMedia();
//                                        callFragment(fragment);
//                                        break;
//                                    case "CSR" :
//                                        fragment = new EngagementCsr();
//                                        callFragment(fragment);
//                                        break;
//                                }
//                                return true;
//                            }
//                        });
//                        popup.show();

                        LayoutInflater inflater2 = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
//
                        // Inflate the custom layout/view
                        View customView2 = inflater2.inflate(R.layout.all_menu_eng,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView2,
                                ViewPager.LayoutParams.MATCH_PARENT,
                                ViewPager.LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button
                        fragmentManager = getFragmentManager();
                        final TableRow tes1 = (TableRow) customView2.findViewById(tableRow1) ;
                        final TableRow tes2 = (TableRow) customView2.findViewById(tableRow2) ;
                        final TableRow tes3 = (TableRow) customView2.findViewById(tableRow3) ;
                        final TableRow tes4 = (TableRow) customView2.findViewById(tableRow4) ;
                        final TableRow tes5 = (TableRow) customView2.findViewById(tableRow5) ;

                        // Set a click listener for the popup window close button
                        tes1.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                fragment = new Engagement();
                                callFragment(fragment);
                                mPopupWindow.dismiss();

                            }
                        });tes2.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            fragment = new EngagementInvestor();
                            callFragment(fragment);
                            mPopupWindow.dismiss();

                        }
                    });tes3.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            fragment = new EngagementInternational();
                            callFragment(fragment);
                            mPopupWindow.dismiss();

                        }
                    });tes4.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            fragment = new EngagementMedia();
                            callFragment(fragment);
                            mPopupWindow.dismiss();

                        }
                    });tes5.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            fragment = new EngagementCsr();
                            callFragment(fragment);
                            mPopupWindow.dismiss();

                        }
                    });



                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);

                        break;
                    case R.id.menu_strategic:
//                        PopupMenu popup_strategic = new PopupMenu(getActivity(),  bottomNavigationView);
//                        // Inflating menu using xml file
//                        popup_strategic.getMenuInflater().inflate(R.menu.strategic_objectives_menu, popup_strategic.getMenu());
//
//                        // registering OnMenuItemClickListener
//                        popup_strategic.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                Toast.makeText(getActivity(),
//                                        "Kamu telah memilih : " + item.getTitle(),
//                                        Toast.LENGTH_SHORT).show();
//                                return true;
//                            }
//                        });
//                        popup_strategic.show();

                        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
//
                        // Inflate the custom layout/view
                        View customView = inflater.inflate(R.layout.str_menu_eng,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView,
                                ViewPager.LayoutParams.MATCH_PARENT,
                                ViewPager.LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button
                        final TableRow tes_1 = (TableRow) customView.findViewById(tableRow1) ;
                        final TableRow tes_2 = (TableRow) customView.findViewById(tableRow2) ;

                        // Set a click listener for the popup window close button
                        tes_1.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                Toast.makeText(getActivity(),
                                        "Kamu telah memilih : Take Over Indonesia Blocks",
                                        Toast.LENGTH_SHORT).show();
                                mPopupWindow.dismiss();

                            }
                        });tes_2.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih : Grass Root Refinery(GRR) Tuban",
                                    Toast.LENGTH_SHORT).show();
                            mPopupWindow.dismiss();

                        }
                    });



                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);
                        break;
                    case R.id.menu_periode:
//                        PopupMenu popup_periode = new PopupMenu(getActivity(),  bottomNavigationView);
//                        // Inflating menu using xml file
//                        popup_periode.getMenuInflater().inflate(R.menu.engagement_periode_menu, popup_periode.getMenu());
//
//                        // registering OnMenuItemClickListener
//                        popup_periode.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            public boolean onMenuItemClick(MenuItem item) {
//                                Toast.makeText(getActivity(),
//                                        "Kamu telah memilih : " + item.getTitle(),
//                                        Toast.LENGTH_SHORT).show();
//                                return true;
//                            }
//                        });
//                        popup_periode.show();


                        LayoutInflater inflater3 = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
//
                        // Inflate the custom layout/view
                        View customView3 = inflater3.inflate(R.layout.per_menu_eng,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView3,
                                ViewPager.LayoutParams.MATCH_PARENT,
                                ViewPager.LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button
                        final TableRow tes1_ = (TableRow) customView3.findViewById(tableRow1) ;
                        final TableRow tes2_ = (TableRow) customView3.findViewById(tableRow2) ;
                        final TableRow tes3_ = (TableRow) customView3.findViewById(tableRow3) ;

                        // Set a click listener for the popup window close button
                        tes1_.setOnClickListener(new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                Toast.makeText(getActivity(),
                                        "Kamu telah memilih : 1 Apr 2017 - 30 Apr 2017",
                                        Toast.LENGTH_SHORT).show();
                                mPopupWindow.dismiss();

                            }
                        });tes2_.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih : 1 Mar 2017 - 31 Mar 2017",
                                    Toast.LENGTH_SHORT).show();
                            mPopupWindow.dismiss();

                        }
                    });tes3_.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Toast.makeText(getActivity(),
                                    "Kamu telah memilih : 1 Feb 2017 - 28 Feb 2017",
                                    Toast.LENGTH_SHORT).show();
                            mPopupWindow.dismiss();

                        }
                    });



                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);

                        break;
                }

                return true;
            }
        } );

        b1 = (Button) view.findViewById(R.id.expandableButtonMedia1);
        b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i("msg", "----------");
                expandableLayout = (ExpandableRelativeLayout) view.findViewById(R.id.expandableLayoutMedia1);
                Log.i("msg2", String.valueOf(expandableLayout));
                expandableLayout.toggle(); // toggle expand and collapse
            }
        });

        b2 = (Button) view.findViewById(R.id.expandableButtonMedia2);
        b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i("msg", "----------");
                expandableLayout = (ExpandableRelativeLayout) view.findViewById(R.id.expandableLayoutMedia2);
                Log.i("msg2", String.valueOf(expandableLayout));
                expandableLayout.toggle(); // toggle expand and collapse
            }
        });

        b3 = (Button) view.findViewById(R.id.expandableButtonMedia3);
        b3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i("msg", "----------");
                expandableLayout = (ExpandableRelativeLayout) view.findViewById(R.id.expandableLayoutMedia3);
                Log.i("msg2", String.valueOf(expandableLayout));
                expandableLayout.toggle(); // toggle expand and collapse
            }
        });

        b4 = (Button) view.findViewById(R.id.expandableButtonMedia4);
        b4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i("msg", "----------");
                expandableLayout = (ExpandableRelativeLayout) view.findViewById(R.id.expandableLayoutMedia4);
                Log.i("msg2", String.valueOf(expandableLayout));
                expandableLayout.toggle(); // toggle expand and collapse
            }
        });

        img1 = (ImageView) view.findViewById(R.id.item_download_media1);
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Downloading pdf...",Toast.LENGTH_SHORT).show();
            }
        });
        img2 = (ImageView) view.findViewById(R.id.item_download_media2);
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Downloading pdf...",Toast.LENGTH_SHORT).show();
            }
        });
        img3 = (ImageView) view.findViewById(R.id.item_download_media3);
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Downloading pdf...",Toast.LENGTH_SHORT).show();
            }
        });
        img4 = (ImageView) view.findViewById(R.id.item_download_media4);
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Downloading pdf...",Toast.LENGTH_SHORT).show();
            }
        });
        img5 = (ImageView) view.findViewById(R.id.item_download_media5);
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Downloading pdf...",Toast.LENGTH_SHORT).show();
            }
        });
        img6 = (ImageView) view.findViewById(R.id.item_download_media6);
        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Downloading pdf...",Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        String firstItem = String.valueOf(spinner1.getSelectedItem());

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (firstItem.equals(String.valueOf(spinner1.getSelectedItem()))) {
                // ToDo when first item is selected
            } else {
                Toast.makeText(getActivity(),
                        "You have selected : " + parent.getItemAtPosition(pos).toString(),
                        Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg) {

        }

    }

    private void callFragment(Fragment fragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();
    }

    public void AddValuesToPIEENTRY(){

        entries.add(new BarEntry(15f, 0));
        entries.add(new BarEntry(14f, 1));
        entries.add(new BarEntry(12f, 2));
        entries.add(new BarEntry(9f, 3));
        entries.add(new BarEntry(9f, 4));
        entries.add(new BarEntry(6f, 5));
        entries.add(new BarEntry(6f, 6));
        entries.add(new BarEntry(6f, 7));
        entries.add(new BarEntry(23f, 9));

    }

    public void AddValuesToPieEntryLabels(){

        PieEntryLabels.add("Tempo");
        PieEntryLabels.add("Kompas");
        PieEntryLabels.add("The Jakarta Post");
        PieEntryLabels.add("Jawa Pos");
        PieEntryLabels.add("BBC");
        PieEntryLabels.add("Time");
        PieEntryLabels.add("Bisnis Indonesia");
        PieEntryLabels.add("Detik.com");
        PieEntryLabels.add("CNN");
    }

    private void AddValuesToPIEENTRYCHAR2() {
        entries_char2.add(new BarEntry(16f, 0));
        entries_char2.add(new BarEntry(15f, 1));
        entries_char2.add(new BarEntry(11f, 2));
        entries_char2.add(new BarEntry(10f, 3));
        entries_char2.add(new BarEntry(10f, 4));
        entries_char2.add(new BarEntry(7f, 5));
        entries_char2.add(new BarEntry(5f, 6));
        entries_char2.add(new BarEntry(5f, 7));
        entries_char2.add(new BarEntry(16f, 9));
    }

    private void AddValuesToPieEntryLabelsChar2() {
        PieEntryLabels_char2.add("Jakob Oetama");
        PieEntryLabels_char2.add("Goenawan Mohamad");
        PieEntryLabels_char2.add("Dahlan Iskan");
        PieEntryLabels_char2.add("Surya Paloh");
        PieEntryLabels_char2.add("Chairul Tanjung");
        PieEntryLabels_char2.add("Eric Tohir");
        PieEntryLabels_char2.add("Harry Tanoesoedibjo");
        PieEntryLabels_char2.add("Staria Narada");
        PieEntryLabels_char2.add("Others");
    }

    private void AddValuesToPIEENTRYCHAR3() {
        entries_char3.add(new BarEntry(14f, 0));
        entries_char3.add(new BarEntry(14f, 1));
        entries_char3.add(new BarEntry(14f, 2));
        entries_char3.add(new BarEntry(9f, 3));
        entries_char3.add(new BarEntry(9f, 4));
        entries_char3.add(new BarEntry(7f, 5));
        entries_char3.add(new BarEntry(7f, 6));
        entries_char3.add(new BarEntry(7f, 7));
        entries_char3.add(new BarEntry(18f, 9));
    }

    private void AddValuesToPieEntryLabelsChar3() {
        PieEntryLabels_char3.add("Mahakam Take Over");
        PieEntryLabels_char3.add("GRR Tuban Project");
        PieEntryLabels_char3.add("RUU Migas");
        PieEntryLabels_char3.add("BUMN Holding");
        PieEntryLabels_char3.add("BUMN Report 2016");
        PieEntryLabels_char3.add("Strategic Plan 2017");
        PieEntryLabels_char3.add("Geothermal Project");
        PieEntryLabels_char3.add("International M&A");
        PieEntryLabels_char3.add("New & Renewable Energy");
    }

    private void listLegendChart1(){
        LegendList l = new LegendList();
        l.setName_legend("Tempo");
        l.setIcon_list(getResources().getColor(R.color.chartlv1));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Time");
        l.setIcon_list(getResources().getColor(R.color.chartlv2));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Kompas");
        l.setIcon_list(getResources().getColor(R.color.chartlv3));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Bisnis Indonesia");
        l.setIcon_list(getResources().getColor(R.color.chartlv4));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("The Jakarta Post");
        l.setIcon_list(getResources().getColor(R.color.chartlv5));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Media Indonesia");
        l.setIcon_list(getResources().getColor(R.color.chartlv6));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Jawa Pos");
        l.setIcon_list(getResources().getColor(R.color.chartlv7));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("Detik.com");
        l.setIcon_list(getResources().getColor(R.color.chartlv8));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("BBC");
        l.setIcon_list(getResources().getColor(R.color.chartlv9));
        LegendsList.add(l);

        l = new LegendList();
        l.setName_legend("CNN");
        l.setIcon_list(getResources().getColor(R.color.chartlv9));
        LegendsList.add(l);
    }

    private void listLegendChart2(){
        LegendList l = new LegendList();
        l.setName_legend("Jakob Oetama");
        l.setIcon_list(getResources().getColor(R.color.chartlv2));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Erick Tohir");
        l.setIcon_list(getResources().getColor(R.color.chartlv3));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Goenawan Mohammad");
        l.setIcon_list(getResources().getColor(R.color.chartlv4));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Harry Tanoesoedibjo");
        l.setIcon_list(getResources().getColor(R.color.chartlv5));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Dahlan Iskan");
        l.setIcon_list(getResources().getColor(R.color.chartlv6));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Staria Narada");
        l.setIcon_list(getResources().getColor(R.color.chartlv7));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Surya Paloh");
        l.setIcon_list(getResources().getColor(R.color.chartlv8));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Others");
        l.setIcon_list(getResources().getColor(R.color.chartlv9));
        LegendsList2.add(l);

        l = new LegendList();
        l.setName_legend("Chairul Tanjung");
        l.setIcon_list(getResources().getColor(R.color.chartlv9));
        LegendsList2.add(l);
    }

    private void listLegendChart3(){
        LegendList l = new LegendList();
        l.setName_legend("Mahakam Take Over");
        l.setIcon_list(getResources().getColor(R.color.chartlv2));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("Strategic Plan 2017");
        l.setIcon_list(getResources().getColor(R.color.chartlv3));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("GRR Tuban Project");
        l.setIcon_list(getResources().getColor(R.color.chartlv4));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("Geothermal Project");
        l.setIcon_list(getResources().getColor(R.color.chartlv5));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("RUU Migas");
        l.setIcon_list(getResources().getColor(R.color.chartlv6));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("International M&A");
        l.setIcon_list(getResources().getColor(R.color.chartlv7));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("BUMN Holding");
        l.setIcon_list(getResources().getColor(R.color.chartlv8));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("New & Renewable Energy");
        l.setIcon_list(getResources().getColor(R.color.chartlv9));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("BUMN Report 2016");
        l.setIcon_list(getResources().getColor(R.color.chartlv8));
        LegendsList3.add(l);

        l = new LegendList();
        l.setName_legend("Others");
        l.setIcon_list(getResources().getColor(R.color.chartlv8));
        LegendsList3.add(l);
    }

}
