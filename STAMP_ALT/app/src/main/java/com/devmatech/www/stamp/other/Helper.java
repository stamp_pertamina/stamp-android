package com.devmatech.www.stamp.other;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Dimas on 6/7/2017.
 */

public class Helper {

    public String LoadJsonDataFromAssets(Context context, String namafile){
        String json = null;
        try{
            InputStream in = context.getAssets().open(namafile);
            int size = in.available();
            byte[] buffer = new byte[size];
            in.read(buffer);
            in.close();
            json = new String(buffer,"UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("error_catch","error at: "+e.getMessage());
        }
        return json;
    }

    public static Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    /**
     * Method untuk mendapatkan width or high of mobile screen
     * 0 = width
     * 1 = height
     */
    public int getSizeScreen(Activity activity, int size){
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        if(size == 0){
            int width = dm.widthPixels;
            return width;
        }else{
            int height = dm.heightPixels;
            return height;
        }
    }

    /*
    * Method untuk mendapatkan width or high of mobile screen
    * 0 = widthDP
    * 1 = heightDP
    * 2 = Density
    * */
    public float getDensityScreen(Activity activity, int size){
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthPixel = dm.widthPixels;
        int heightPixel = dm.heightPixels;
        float scalefactor = dm.density;
        if(size == 0){
            float widthDp = (float)widthPixel/scalefactor;
            return widthDp;
        }else if(size == 1){
            float heightDp = (float)heightPixel/scalefactor;
            return heightDp;
        }else{
            float widthDp = widthPixel/scalefactor;
            float heightDp = heightPixel/scalefactor;
            float smallestWidth = Math.min(widthDp, heightDp);
            return smallestWidth;
        }
    }

    public double getScreenInchies(Activity activity){
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width=dm.widthPixels;
        int height=dm.heightPixels;
        double wi=(double)width/(double)dm.xdpi;
        double hi=(double)height/(double)dm.ydpi;
        double x = Math.pow(wi,2);
        double y = Math.pow(hi,2);
        double screenInches = Math.sqrt(x+y);
        return  screenInches;
    }
}
