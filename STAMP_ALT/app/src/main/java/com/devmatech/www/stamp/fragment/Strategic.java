package com.devmatech.www.stamp.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.activity.Detail_Strategic;

import static android.R.attr.category;

/**
 * Created by Whelly on 30/05/2017.
 */

public class Strategic extends Fragment {

    public Strategic(){}
    RelativeLayout view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = (RelativeLayout) inflater.inflate(R.layout.strategic_main, container, false);

        getActivity().setTitle("Strategic Objectives");
        String[] strategicArray = {"Maintaning Domestic Oil and Gas Production","Good Corporate Govermance","Company's Value Improvment","Ensuring Stable Cash Flow",
                "Effective Utilization of CSR Fund","Support on Energy Holding Establishment","Good Corporate Govermance","Mahakam Block Take Over",
                "Support on Nasional Energy Security","Support on Achieving National Mix Target"};
        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.list_strategic, strategicArray);

        ListView listView = (ListView) view.findViewById(R.id.strategic_list);
        ArrayAdapter<String> array = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1);
        for (String str : strategicArray)
            array.add(str);
        listView.setAdapter(array);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int potition, long l) {
//                Toast.makeText(getActivity(), "Stop Clicking me", Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(getActivity(), com.devmatech.www.stamp.activity.Detail_Strategic.class);
                startActivity(myIntent);
            }
        });

        return view;
    }
}
