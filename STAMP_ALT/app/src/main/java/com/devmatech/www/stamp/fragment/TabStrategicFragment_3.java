package com.devmatech.www.stamp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.activity.DetailIndividual;
import com.devmatech.www.stamp.adapter.IndividualListAdapter;
import com.devmatech.www.stamp.adapter.StrategicAdapter;
import com.devmatech.www.stamp.models.IndividualsProfile;

import java.util.ArrayList;

/**
 * Created by Whelly on 05/06/2017.
 */

public class TabStrategicFragment_3 extends Fragment {

    LinearLayout view;
    ListView listView;
    ListAdapter listAdapter;
    String ListItemsName[] = new String[]{
            "Mr. Rachmat Hardadi",
            "Mr. Michael Sihombing",
            "Mr. Budi Santoso Syarif",
            "Mr. Ganades Panjaitan",
            "Mr. Budi Santoso Syarif"
    };
    String ListItemsDesc[] = new String[]{
            "Directory of Mega Project Refinery & Petrochemical",
            "SVP of Refinery Operation",
            "SVP of Refinery Technology",
            "Cheif of Legal Counsel and Compliance",
            "SPV of Refinery Technology"
    };
    Integer ImageName[] = {
            R.drawable.john,
            R.drawable.gambar_kosong,
            R.drawable.gambar_kosong,
            R.drawable.gambar_kosong,
            R.drawable.gambar_kosong
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        view = (RelativeLayout) inflater.inflate(R.layout.tab_strategic_fragment_3, container, false);
//        listView = (ListView)view.findViewById(R.id.list_agenda);
//        listAgenda();
//
//        adapter = new IndividualListAdapter(itemList, getActivity().getBaseContext());
//        listView.setAdapter(adapter);
//        return inflater.inflate(R.layout.tab_strategic_fragment_3, container, false);
        view = (LinearLayout) inflater.inflate(R.layout.tab_strategic_fragment_3, container, false);
        listView = (ListView)view.findViewById(R.id.list_agenda);
        listAdapter = new StrategicAdapter(getActivity(),ListItemsName,ListItemsDesc, ImageName);

        listView.setAdapter(listAdapter);

//
////        adapter = new IndividualListAdapter(itemList, getActivity().getBaseContext());
//        listView.setAdapter(adapter);

       return view;
    }

}
