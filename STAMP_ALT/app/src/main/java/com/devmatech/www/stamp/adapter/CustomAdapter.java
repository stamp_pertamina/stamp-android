package com.devmatech.www.stamp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.devmatech.www.stamp.R;

/**
 * Created by Whelly on 30/05/2017.
 */

public class CustomAdapter extends BaseAdapter {

    private Context context;
    private String [] app_name;
    private int [] app_icon;

    public CustomAdapter(Context context, String [] app_name, int [] app_icon) {

        this.context=context;
        this.app_icon=app_icon;
        this.app_name=app_name;
    }

    @Override
    public int getCount() {

        return app_name.length;
    }

    @Override
    public Object getItem(int position) {

        return position;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=inflater.inflate(R.layout.home_grid, null);
        TextView name=(TextView)convertView.findViewById(R.id.nameHome);
        ImageView icon=(ImageView)convertView.findViewById(R.id.imageHome);
        name.setText(app_name[position]);
        icon.setImageResource(app_icon[position]);
        return convertView;
    }
}
