package com.devmatech.www.stamp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.devmatech.www.stamp.R;

/**
 * Created by Dimas on 6/1/2017.
 */

public class InteractionAdapter extends ArrayAdapter<String>{
    private final Activity context;
    private final String[] itemname;


    public InteractionAdapter (Activity context, String[] itemname){
        super(context, R.layout.list_interaction, itemname);
        this.context = context;
        this.itemname = itemname;
    }

    public View getView(int position, View view, ViewGroup parent) {
//
//        View rowView=inflater.inflate(R.layout.list_interaction, null,true);

        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.list_interaction, null,true);

        TextView txtName = (TextView) rowView.findViewById(R.id.name_report);
        TextView txtDate = (TextView) rowView.findViewById(R.id.date_report);
        TextView txtContent = (TextView) rowView.findViewById(R.id.content_reports);

        txtName.setText(itemname[position]);
        return rowView;

    };

}
