package com.devmatech.www.stamp.adapter;

/**
 * Created by cahyo on 14/06/2017.
 */
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.devmatech.www.stamp.R;
import com.devmatech.www.stamp.models.OPModel;
import com.devmatech.www.stamp.models.Orgdetil;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;


public class OrgListAdapter extends BaseAdapter {

    private ArrayList<OPModel> listProfile = new ArrayList<>();
    private Context context;
    private LayoutInflater mInflater;
    int hide = 0;

    public OrgListAdapter(ArrayList<OPModel> listProfile, Context context) {
        this.listProfile = listProfile;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        hide = listProfile.size() - 3;
    }

    public void showAll(){
        hide = 0;
        notifyDataSetChanged();
        notifyDataSetInvalidated();
    }

    @Override
    public int getCount() {
        return listProfile.size() - hide;
    }

    @Override
    public Object getItem(int position) { return listProfile.get(position); }

    @Override
    public long getItemId(int position) { return position; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemHolder holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.org_data, parent,false);
            holder = new ItemHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ItemHolder)convertView.getTag();
        }
        final OPModel item = listProfile.get(position);
        holder.profile_nama.setText(item.getStakeholderName());
        holder.profile_jabatan.setText(item.getOrganizationType());
        Uri uri;
        if(item.getId().length() > 0){
//            File file = new File(URI.create("file:///android_asset/OP/" + item.getId()+".jpg"));
//            if (file.exists()){
                uri = Uri.parse("file:///android_asset/OP/" + item.getId()+".jpg");
//            }else{
//                uri = Uri.parse("file:///android_asset/no_image.jpeg");
//            }
        }else {
            uri = Uri.parse("file:///android_asset/no_image.jpeg");
        }
        //link url untuk akses gambar
        //nanti kalau sudah siap langsung tambahkan nama file di belakang url
        String imageurl = "";
        Picasso.with(context).load(uri).resize(100,100)
                .placeholder(R.drawable.progress_animation).centerCrop().into(holder.profile_avatar);

        return convertView;
    }

    class ItemHolder{
        private TextView profile_nama, profile_jabatan;
        ImageView profile_avatar;
        public ItemHolder(View view) {
            profile_nama = (TextView)view.findViewById(R.id.item_profile_nama);
            profile_jabatan = (TextView)view.findViewById(R.id.item_profile_jabatan);;
            profile_avatar = (ImageView)view.findViewById(R.id.item_profile_image);
        }
    }
}
