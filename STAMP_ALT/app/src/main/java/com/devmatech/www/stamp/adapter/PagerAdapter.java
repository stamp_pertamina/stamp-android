package com.devmatech.www.stamp.adapter;

/**
 * Created by Whelly on 31/05/2017.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.devmatech.www.stamp.fragment.TabFragment;
import com.devmatech.www.stamp.fragment.TabStrategicFragment_1;
import com.devmatech.www.stamp.fragment.TabStrategicFragment_2;
import com.devmatech.www.stamp.fragment.TabStrategicFragment_3;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int numOfTabs;

    public PagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

//        return TabFragment.getInstance(position);
        switch (position) {
            case 0:
                TabStrategicFragment_1 tab1 = new TabStrategicFragment_1();
                return tab1;
            case 1:
                TabStrategicFragment_2 tab2 = new TabStrategicFragment_2();
                return tab2;
            case 2:
                TabStrategicFragment_3 tab3 = new TabStrategicFragment_3();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }


}
